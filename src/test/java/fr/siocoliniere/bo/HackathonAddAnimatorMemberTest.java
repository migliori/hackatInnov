package fr.siocoliniere.bo;

import org.junit.jupiter.api.BeforeEach;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HackathonAddAnimatorMemberTest {
    private Hackathon hacka ;
    @BeforeEach
    void init(){
        hacka = new Hackathon("name", "name1", "desc1", "place","target", "room1", 2, new Date(52126142), 2);

    }

    @org.junit.jupiter.api.Test
    void testAddJuryMember() {
        Member member = new Member(1,"lastname","fisrtname", "phone", "email");
        hacka.addAnimatorMember(member);

        assertTrue(hacka.getAnimatorMembers().contains(member),"Erreur : un nouveau membre ajouté ");
    }
}