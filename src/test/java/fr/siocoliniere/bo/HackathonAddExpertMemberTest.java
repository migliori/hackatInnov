package fr.siocoliniere.bo;

import org.junit.jupiter.api.BeforeEach;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HackathonAddExpertMemberTest {
    private Hackathon hacka ;
    @BeforeEach
    void init(){
        hacka = new Hackathon("name", "name1", "desc1", "place","target", "room1", 2, new Date(52126142), 2);

    }

    @org.junit.jupiter.api.Test
    void testAddJuryMember() {
        Expert expert = new Expert("lastname","fisrtname", "phone", "email", "job");
        hacka.addAnimatorMember(expert);

        assertTrue(hacka.getAnimatorMembers().contains(expert),"Erreur : un nouveau membre ajouté ");
    }
}