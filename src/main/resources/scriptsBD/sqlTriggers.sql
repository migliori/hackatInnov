CREATE
OR REPLACE FUNCTION createmembreuser(unlastname character varying, unfirstname character varying, unphone character varying, unemail character varying, unpassword character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
unIdMembre integer;
BEGIN
insert into membre(lastname, firstname, phone)
values (unLastName, unFirstName, unPhone);
select
into unIdMembre lastval();
insert into "user"(email, password, roles, idmembre)
values (unEmail, unPassword, '["ROLE_USER"]', unIdMembre);

RETURN unIdMembre;
END;
$function$
;

CREATE
OR REPLACE FUNCTION initcaphackathon()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
	    NEW.name
:= INITCAP(LOWER(NEW.name));

RETURN NEW;
END;
$function$
;

CREATE
OR REPLACE FUNCTION initcapmember()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
        NEW.lastname
:= INITCAP(LOWER(NEW.lastname));
        NEW.firstname
:= INITCAP(LOWER(NEW.firstname));

RETURN NEW;
END;
    $function$
;

CREATE
OR REPLACE FUNCTION is_inscrit()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
	DECLARE
canInsert inscription%ROWTYPE;
        inscrit
int;
BEGIN
select
into inscrit id
from inscription
where idmembre = new.idmembre and idhackathon = new.idhackathon;
if
inscrit is null then
        	canInsert = new;
else
        	canInsert = null;
end if;

return canInsert;
END;
$function$
;

CREATE
OR REPLACE FUNCTION updatedata()
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
ligne hackathon%rowtype;
begin
FOR ligne in
SELECT *
FROM hackathon LOOP
update hackathon
set "name" = initcap(lower(ligne.name))
where id = ligne.id;
END loop;
return 0;
end
$function$
;

CREATE
OR REPLACE FUNCTION updatemembreuser(iddumembre integer, unlastname character varying, unfirstname character varying, unphone character varying, unemail character varying, unpassword character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
BEGIN
UPDATE membre
SET lastname  = unLastName,
    firstname = unFirstname,
    phone     = unPhone
WHERE id = idDuMembre;
UPDATE "user"
SET email    = unEmail,
    password = unPassword
WHERE idmembre = idDuMembre;

RETURN idDuMembre;
END;
$function$
;

create trigger trig_before_insert_membre_initcapmember
    before
        insert
    on
        membre
    for each row execute function initcapmember();
create trigger trig_bef_initcap_member_update
    before
        update
    on
        membre
    for each row execute function initcapmember();


create trigger trig_before_insert_hackathon_initcaphackathon
    before
        insert
    on
        hackathon
    for each row execute function initcaphackathon();
create trigger trig_bef_initcap_hackathon_update
    before
        update
    on
        hackathon
    for each row execute function initcaphackathon();

create trigger trig_before_insert_inscription
    before
        insert
    on
        inscription
    for each row execute function is_inscrit();