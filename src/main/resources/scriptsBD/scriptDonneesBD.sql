INSERT INTO membre (lastname, firstname, phone, "type")
VALUES ('Goldman', 'Jean-Jacques', '0606060607', 'animateur'),
       ('Rufino', 'Hugo', '0606060608', 'jury'),
       ('Calas', 'Dorian', '0606060609', 'animateur'),
       ('Ritou', 'Lucas', '0606060614', 'jury'),
       ('Situation', 'Lena', '0606060610', 'participant'),
       ('Jean', 'Cesar', '0606060611', 'participant'),
       ('Quinn', 'Eva', '0606060612', 'animateur'),
       ('Michou', 'Elsa', '0606060613', 'expert');

INSERT INTO "user" (email, "password", roles, idmembre)
VALUES ('jGoldman@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 1),
       ('hRufino@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 2),
       ('dCalas@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 3),
       ('lRitou@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 4),
       ('sLena@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 5),
       ('jCesar@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 6),
       ('qEva@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 7),
       ('mElsa@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 8);

INSERT INTO organisateur (id)
VALUES (3),
       (6),
       (8);

INSERT INTO typeinscription (libelle)
VALUES ('Sur validation'),
       ('Automatique');

INSERT INTO hackathon ("name", "date", "location", target, topic, description, nbentrant, idtypeinscription, canvote,
                       caninscription, canposition, idorganisateur)
VALUES ('Hackact', '2037-03-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        19, 1, 0, 0, 0, 3),
       ('Hack''Web', '2037-03-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        19, 1, 0, 0, 0, 3),
       ('Hack''Innov', '2076-01-01', 'Nantes', 'service publique', 'Services publics et Innovation',
        'Ce hackathon vise à réunir les citoyens d’une communauté pour développer des solutions afin d’améliorer les services publics offerts aux citoyens',
        15, 2, 0, 0, 0, 8),
       ('Hack''Consul', '2078-11-07', 'Nantes', 'métier consultant', 'Réinventer un métier',
        'Ce hackathon vise à réunir des étudiants afin de réinventer le métier de Consultant', 9, 1, 0, 0, 0, 6),
       ('Hack''Orga', '2444-06-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        44, 2, 0, 0, 0, 3);

INSERT INTO animateur (id)
VALUES (1),
       (3),
       (7);

INSERT INTO animateurcompose (idanimateur, idhackathon)
VALUES (1, 3),
       (3, 4),
       (7, 2);

INSERT INTO expert (id, job)
VALUES (8, 'Dans.eur.euse');

INSERT INTO expertcompose (idexpert, idhackathon)
VALUES (8, 5);

INSERT INTO jury (id)
VALUES (2),
       (4);

INSERT INTO jurycompose (idjury, idhackathon)
VALUES (2, 3),
       (4, 4);

INSERT INTO participant (id)
VALUES (5),
       (6);

INSERT INTO projet (libelle, description, isvalid, idhackathon)
VALUES ('l''exemple de la bouilloire', 'Quand vous êtes harassés de fatigue, pensez toujours à l’exemple de la bouilloire. Elle a beau avoir le couvercle en ébullition, cela ne l’empêche pas de chanter.', 0, 2),
       ('base du coton',
        'Le coton est une fibre végétale qui entoure les graines des cotonniers « véritables » (Gossypium sp.), arbustes de la famille des Malvacées. Cette fibre, constituée de cellulose presque pure, est généralement transformée en fil qui est tissé pour fabriquer des tissus. Le coton est la fibre naturelle la plus produite dans le monde, principalement en Chine et en Inde. Depuis le xixe siècle, il constitue, grâce aux progrès de l''industrialisation et de l''agronomie, la première fibre textile du monde (près de la moitié de la consommation mondiale de fibres textiles)',
        0, 3),
       ('masquer les coups dans la statue',
        'La sculpture sur pierre est une technique particulière de sculpture en ce qu''elle désigne un mode de fabrication d''une œuvre différente du modelage en argile ou en fonte. Le terme ne doit pas être confondu avec l''activité des tailleurs de pierre qui façonnent des blocs de pierre destinés à la sculpture mais aussi à l''architecture, le bâtiment ou le génie civil. C''est aussi une expression utilisée par les archéologues, les historiens et anthropologues pour décrire la fabrication des pétroglyphe.',
        0, 2),
       ('les réflexions transitives', 'Description du projet "les reflexions transitives"', 0, 1),
       ('la barbe de troie', 'Description du projet "la barbe de troie"', 0, 1),
       ('courir avant d''atteindre le cheval', 'Description du projet "courir avant d''atteindre le cheval"', 0, 4),
       ('les métiers² de l''horticulture avec une base de données',
        'Description du projet "les métiers de l''horticulture avec une base de données"', 0, 5);

INSERT INTO equipe (nom, idprojet, idchef)
VALUES ('migliori', 4, NULL),
       ('katcentkat', 5, NULL),
       ('l''attaque des titouans', 7, NULL);

INSERT INTO inscription (dateinscription, isvalid, idmembre, idhackathon, idprojet, idequipe)
VALUES ('2022-02-28', 0, 5, 1, 4, 1),
       ('2021-11-24', 0, 6, 2, 7, 3);

INSERT INTO vote (idexpert, idprojet)
VALUES (8, 7);