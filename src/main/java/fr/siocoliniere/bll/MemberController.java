package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOMembre;

public class MemberController
{
    private static MemberController instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return MemberController
     */
    public static synchronized MemberController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new MemberController();
        }
        return instanceCtrl;
    }

    /**
     * Constructeur de la classe MemberController
     */
    private MemberController()
    {}

    public boolean hasSameEmailInList(String email)
    {
        boolean res = false;
        try
        {
            res =  DAOMembre.getInstance().emailExist(email);
        }
        catch(DALException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    public boolean hasSameEmailInList(Member expert, String email)
    {
        boolean res = false;
        try
        {
            res =  DAOMembre.getInstance().emailExist(expert, email);
        }
        catch(DALException e)
        {
            e.printStackTrace();
        }
        return res;
    }

}
