package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOAnimator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AnimatorController
{
    private static AnimatorController instanceCtrl;
    private List<Member> listOfAnimators;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return AnimatorController
     */
    public static synchronized AnimatorController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new AnimatorController();
        }
        return instanceCtrl;
    }


    /**
     * Constructeur de la classe AnimatorController
     */
    private AnimatorController()
    {
        try
        {
            listOfAnimators = DAOAnimator.getInstance().getAll();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * permet de recuperer tout les animateurs ranger par leur nom de famille
     * @return List<Member>
     */
    public List<Member> getAll()
    {
        Collections.sort(listOfAnimators, Comparator.comparing(Member::getLastname));
        return listOfAnimators;
    }

    /**
     * permet de recuperer les animateurs qui ne sont pas dans le hackathon passé en parametre
     * @param hackathonManaged
     * @return
     */
    public List<Member> getWithout(Hackathon hackathonManaged)
    {
        List<Member> animators = null;
        try
        {
            animators = DAOAnimator.getInstance().getAnimatorWithout(hackathonManaged.getId());
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }

        return animators;
    }

    /**
     * permet de sauvegarder un animateur
     * @param animator
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     * @return
     */
    public Member saveAnimatorStandalone(Member animator, String lastname, String firstname,String phone, String email)
    {
        if (animator !=null)
        {
            animator.setLastname(lastname);
            animator.setFirstname(firstname);
            animator.setPhone(phone);
            animator.setEmail(email);

            try
            {
                DAOAnimator.getInstance().updateSettings(animator);
            }
            catch(DALException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            animator = new Member(lastname,firstname,phone,email);
            listOfAnimators.add(animator);

            try
            {
                DAOAnimator.getInstance().saveSettings(animator);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }

        return animator;
    }
}

