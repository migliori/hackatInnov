package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOOrganisateur;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrganisateurController
{
    private static OrganisateurController instanceCtrl;
    private List<Member> listOfMember;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return organisateurController
     */
    public static synchronized OrganisateurController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new OrganisateurController();
        }
        return instanceCtrl;
    }

    /**
     * constructeur de la classe OrganisateurController
     */
    private OrganisateurController()
    {
        try
        {
            listOfMember = DAOOrganisateur.getInstance().getAll();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * permet de recuperer tout les organisateurs ranger par leur nom de famille
     * @return List<Expert>
     */
    public List<Member> getAll()
    {
        Collections.sort(listOfMember, Comparator.comparing(Member::getLastname));
        return listOfMember;
    }

    /**
     * permet de sauvegarder un organisateur
     * @param organisateurManaged
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     * @return
     */
    public Member saveOrganisateurStandalone(Member organisateurManaged, String lastname, String firstname, String phone, String email)
    {
        if(organisateurManaged != null)
        {
            organisateurManaged.setLastname(lastname);
            organisateurManaged.setFirstname(firstname);
            organisateurManaged.setPhone(phone);
            organisateurManaged.setEmail(email);

            try
            {
                DAOOrganisateur.getInstance().update(organisateurManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            organisateurManaged = new Member(lastname, firstname, phone, email);
            listOfMember.add(organisateurManaged);

            try
            {
                DAOOrganisateur.getInstance().save(organisateurManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }

        return organisateurManaged;
    }
}
