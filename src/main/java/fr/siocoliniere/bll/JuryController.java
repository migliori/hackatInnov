package fr.siocoliniere.bll;


import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOJury;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class JuryController
{
    private static JuryController instanceCtrl;
    private List<Member> listOfMember;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return JuryController
     */
    public static synchronized JuryController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new JuryController();
        }
        return instanceCtrl;
    }

    /**
     * constructeur de la classe JuryController
     */
    private JuryController()
    {
        try
        {

            listOfMember = DAOJury.getInstance().getAll();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'ensemble des jury trié par nom de famille
     * @return la liste des hackathons
     */
    public List<Member> getAll()
    {
        Collections.sort(listOfMember, Comparator.comparing(Member::getLastname));
        return listOfMember;
    }

    /**
     * Permet de récupérer l'ensemble des jurys qui ne sont pas dans la composition du hackathon actuel
     * @return la liste des jury qui ne sont pas dans le hackathon
     */
    public List<Member> getWithout(Hackathon hackathonManaged)
    {
        List<Member> juries = null;
        try
        {
            juries = DAOJury.getInstance().getJuryWithout(hackathonManaged.getId());
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
        return juries;
    }

    /**
     * permet de sauvegarder un jury
     * @param juryManaged
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     * @return
     */
    public Member saveJuryStandalone(Member juryManaged, String lastname, String firstname, String phone, String email)
    {
        if(juryManaged != null)
        {
            juryManaged.setLastname(lastname);
            juryManaged.setFirstname(firstname);
            juryManaged.setPhone(phone);
            juryManaged.setEmail(email);

            try
            {
                DAOJury.getInstance().update(juryManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            juryManaged = new Member(lastname, firstname, phone, email);
            listOfMember.add(juryManaged);

            try
            {
                DAOJury.getInstance().save(juryManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }

        return juryManaged;
    }
}