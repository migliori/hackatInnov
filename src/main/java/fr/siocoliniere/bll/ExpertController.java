package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOExpert;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExpertController
{
    private List<Expert> listOfExpert;
    private static ExpertController instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return ExpertController
     */
    public static synchronized ExpertController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new ExpertController();
        }
        return instanceCtrl;
    }

    /**
     * constructeur de la classe ExpertController
     */
    private ExpertController()
    {
        try
        {
            this.listOfExpert = DAOExpert.getInstance().getAll();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * permet de recuperer tout les expert ranger par leur nom de famille
     * @return List<Expert>
     */
    public List<Expert> getAll()
    {
        Collections.sort(listOfExpert, Comparator.comparing(Expert::getLastname));
        return this.listOfExpert;
    }

    /**
     * permet de recuperer les experts qui ne sont pas dans le hackathon passé en parametre
     * @param hackathonManaged
     * @return
     */
    public List<Expert> getWithout(Hackathon hackathonManaged)
    {
        List<Expert> experts = null;
        try {
            experts = DAOExpert.getInstance().getExpertWithout(hackathonManaged.getId());
        } catch (DALException e) {
            e.printStackTrace();
        }
        return experts;
    }

    /**
     * permet de sauvegarder un expert
     * @param expertManaged
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     * @param job
     * @return
     */
    public Expert saveExpertStandalone(Expert expertManaged, String lastname, String firstname, String phone, String email, String job)
    {
        if(expertManaged != null)
        {
            expertManaged.setLastname(lastname);
            expertManaged.setFirstname(firstname);
            expertManaged.setPhone(phone);
            expertManaged.setEmail(email);
            expertManaged.setJob(job);

            try
            {
                DAOExpert.getInstance().update(expertManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            expertManaged = new Expert(lastname, firstname, phone, email, job);
            listOfExpert.add(expertManaged);

            try
            {
                DAOExpert.getInstance().save(expertManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }

        return expertManaged;
    }
}
