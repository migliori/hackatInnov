package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;

import java.util.*;

public class HackathonController
{
    private static HackathonController instanceCtrl;
    private List<Hackathon> listOfHackathons;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return HackathonController
     */
    public static synchronized HackathonController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new HackathonController();
        }

        return instanceCtrl;
    }



    /**
     * Constructeur de la classe HackathonController
     */
    private HackathonController()
    {
        try
        {
            listOfHackathons = DAOHackathon.getInstance().getAll();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'ensemble des hackathons trié par nom
     * @return la liste des hackathons
     */
    public List<Hackathon> getAll()
    {
        Collections.sort(listOfHackathons, Comparator.comparing(Hackathon::getName));
        return listOfHackathons;
    }

    /**
     * Permet de sauvegarder les settings d'un hackathon
     * @param hackathonManaged
     *
     */
    public Hackathon saveHackathonStandalone(Hackathon hackathonManaged, String name, String topic, String description, String place, String target, String room, Integer nbrParticipants, Date dateHackathon, Member organisateur)
    {
        if (hackathonManaged != null)
        {
            hackathonManaged.setName(name);
            hackathonManaged.setTopic(topic);
            hackathonManaged.setDescription(description);
            hackathonManaged.setPlace(place);
            hackathonManaged.setTarget(target);
            hackathonManaged.setRoom(room);
            hackathonManaged.setNbrParticipants(nbrParticipants);
            hackathonManaged.setDate(dateHackathon);
            hackathonManaged.setIdOrganisateur(organisateur.getId());


            try
            {
                DAOHackathon.getInstance().updateSettings(hackathonManaged);

            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            hackathonManaged = new Hackathon(name, topic, description, place, target,room,nbrParticipants, dateHackathon, organisateur.getId());
            listOfHackathons.add(hackathonManaged);

            try
            {
                DAOHackathon.getInstance().saveSettings(hackathonManaged);
            }
            catch (DALException e)
            {
                e.printStackTrace();
            }
        }

        return hackathonManaged;
    }

    /**
     * Permet de persister l'ensemble des jury du hackathon
     * @param hackathonManaged : hackathon
     * @param juries : liste de jury
     */
    public void saveJuriesByIdHackathon(Hackathon hackathonManaged,List<Member> juries)
    {
        hackathonManaged.clearJury();
        hackathonManaged.setJuryMembers(juries);

        try
        {
            DAOHackathon.getInstance().deleteAllJuryComposeOfHackathon(hackathonManaged.getId());

            for (Member oneJury :juries)
            {
                DAOHackathon.getInstance().saveJury(hackathonManaged.getId(),oneJury);
            }

        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Permet de persister l'ensemble des animateurs du hackathon
     * @param hackathonManaged : hackathon
     * @param animators : liste d'animateur
     */
    public void saveAnimatorsByIdHackathon(Hackathon hackathonManaged,List<Member> animators)
    {
        hackathonManaged.clearAnimator();
        hackathonManaged.setAnimatorMembers(animators);

        try
        {
            DAOHackathon.getInstance().deleteAllAnimatorComposeOfHackathon(hackathonManaged.getId());

            for (Member oneAnimator :animators)
            {
                DAOHackathon.getInstance().saveAnimator(hackathonManaged.getId(),oneAnimator);
            }

        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Permet de persister l'ensemble des experts du hackathon
     * @param hackathonManaged : hackathon
     * @param experts : liste d'expert
     */
    public void saveExpertsByIdHackathon(Hackathon hackathonManaged,List<Expert> experts)
    {
        hackathonManaged.clearExpert();
        hackathonManaged.setExpertMembers(experts);

        try
        {
            DAOHackathon.getInstance().deleteAllExpertComposeOfHackathon(hackathonManaged.getId());

            for (Expert oneMember : experts)
            {
                DAOHackathon.getInstance().saveExpert(hackathonManaged.getId(), oneMember);
            }

        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * permet de recuperer tout les types de rooms disponible pour un hackthons
     * @return ArrayList<String>
     */
    public ArrayList<String> getRoom()
    {
        ArrayList<String> rooms = new ArrayList<>();

        try
        {
            rooms = DAOHackathon.getInstance().getRoom();
        }
        catch (DALException e)
        {
            e.printStackTrace();
        }

        return rooms;
    }

    /**
     * Permet de supprimer un hackathon
     * @param hackathonManaged : hackathon
     */
    public void deleteHackathon(Hackathon hackathonManaged)
    {
        try
        {
            DAOHackathon.getInstance().deleteAllExpertComposeOfHackathon(hackathonManaged.getId());
            DAOHackathon.getInstance().deleteAllAnimatorComposeOfHackathon(hackathonManaged.getId());
            DAOHackathon.getInstance().deleteAllJuryComposeOfHackathon(hackathonManaged.getId());
//            DAOHackathon.getInstance().deleteEquipe(hackathonManaged.getId());
            DAOHackathon.getInstance().deleteInscription(hackathonManaged.getId());
            DAOHackathon.getInstance().deleteProjet(hackathonManaged.getId());
            DAOHackathon.getInstance().deleteHackathonById(hackathonManaged.getId());
            this.listOfHackathons.remove(hackathonManaged);

        }
        catch (DALException e)
        {
            e.printStackTrace();
        }
    }
}