package fr.siocoliniere.bll;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

public class GlobalController
{
    private static GlobalController instanceCtrl;
    private Properties prop;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return GlobalController
     */
    public static synchronized GlobalController getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new GlobalController();
        }
        return instanceCtrl;
    }

    /**
     * constructeur de la classe GlobalController
     */
    private GlobalController()
    {}

    /**
     * permet de charger le fichier de la langue passer en parametre
     * @param lang
     */
    public void loadLanguageByName(String lang)
    {
        try
        {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();

            prop = new Properties();

            InputStream resourceStream = loader.getResourceAsStream(lang + ".properties");
            prop.load(resourceStream);
        }
        catch (IOException io)
        {
            io.printStackTrace();
        }
    }

    /**
     * renvoie vrai si la valeur passer en parametre est un integer, faux sinon
     * @param value
     * @return boolean
     */
    public boolean tryParseInt(String value)
    {
        boolean res;

        try
        {
            Integer.parseInt(value);
            res = true;
        }
        catch (NumberFormatException e)
        {
            res = false;
        }

        return res;
    }

    /**
     * renvoie vrai si la valeur passer en parametre est un date valide, faux sinon
     * @param input
     * @return boolean
     */
    public boolean isValidDate(String input)
    {
        String formatString = "dd/MM/yyyy";
        boolean res;
        try
        {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            format.setLenient(false);
            format.parse(input);
            res = true;
        }
        catch (ParseException | IllegalArgumentException e)
        {
            res = false;
        }

        return res;
    }

    /**
     * permet de recuperer une ligne du fichier charger par la method loadLanguageByName(String): void
     * @param key
     * @return
     */
    public String getProperty(String key)
    {
        return prop.getProperty(key);
    }
}
