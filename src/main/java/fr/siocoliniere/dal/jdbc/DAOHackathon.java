package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOHackathon extends DAO<Hackathon>
{
    private static DAOHackathon instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static synchronized DAOHackathon getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new DAOHackathon();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectIdByName= "select hackathon.id from hackathon where hackathon.name = ?";
    private static final String sqlSelectAll = "select hackathon.id, hackathon.name, hackathon.topic, hackathon.description, hackathon.location, hackathon.target, typeinscription.libelle, hackathon.nbentrant, hackathon.date, hackathon.idorganisateur from hackathon inner join typeinscription on hackathon.idtypeinscription = typeinscription.id";
    private static final String sqlSelectOneById = "select hackathon.id, hackathon.name, hackathon.topic, hackathon.description, hackathon.location, hackathon.target, hackathon.idtypeinscription, hackathon.nbrentrant, hackathon.date from hackathon where hackathon.id = ?";
    private static final String sqlSelectRoomState = "select typeinscription.libelle from typeinscription";
    private static final String sqlSelectIdByType = "select typeinscription.id from typeinscription where typeinscription.libelle = ?";
    private static final String sqlUpdateOne = "update hackathon set name = ?, date = ?, topic = ?, description = ?, location = ?, target = ?, idtypeinscription = ? , nbentrant = ?, idorganisateur = ? where id = ?";
    private static  final String sqlInsertOne = "insert into hackathon(name, date, topic, description, location, target, idtypeinscription, nbentrant, idorganisateur) values (?,?, ?, ?,?,?,?,?,?)";
    private static final String sqlInsertJuryCompose = "insert into jurycompose(idjury, idhackathon) values (?,?)";
    private static final String sqlInsertAnimateurCompose = "insert into animateurcompose(idanimateur, idhackathon) values (?,?)";
    private static final String sqlDeleteAllComposeJury = "delete from jurycompose where idhackathon = ? ";
    private static final String sqlDeleteAllComposeAnimator = "delete from animateurcompose where idhackathon = ? ";
    private static final String sqlInsertExpertCompose = "insert into expertcompose(idexpert, idhackathon) values (?,?)";
    private static final String sqlDeleteAllComposeExpert = "delete from expertcompose where idhackathon = ? ";
    private static final String sqlDeleteHackathonById = "delete from hackathon where id = ?";
    private static final String sqlDeleteInscription = "delete from inscription where idhackathon = ?";
    private static final String sqlDeleteEquipe = "delete from equipe where idhackathon = ?";
    private static final String sqlDeleteprojet = "delete from projet where idhackathon = ?";

    /**
     * Permet de supprimer la composition du jury du hackathon
     * @param id : id du hackathon
     * @throws DALException
     */
    public void deleteAllJuryComposeOfHackathon(int id) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllComposeJury);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de supprimer la composition des animateurs du hackathon
     * @param id : id du hackathon
     * @throws DALException
     */
    public void deleteAllAnimatorComposeOfHackathon(int id) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllComposeAnimator);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de supprimer la composition des experts du hackathon
     * @param id : id du hackathon
     * @throws DALException
     */
    public void deleteAllExpertComposeOfHackathon(int id) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllComposeExpert);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de récupérer tous les hackathons stockés dans la table Hackathon
     * @return la liste des hackathons
     * @throws DALException
     */
    @Override
    public List<Hackathon> getAll() throws DALException
    {
        List<Hackathon> lstHackathon = new ArrayList<>();
        List<Member> lstAnimators;
        List<Member> lstJury;
        List<Expert> lstExpert;

        try
        {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAll);

            while (rs.next())
            {
                //instanciation du hackathon
                Hackathon hacka = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"), rs.getString("location"), rs.getString("target"),rs.getString("libelle"),rs.getInt("nbentrant"), rs.getDate("date"), rs.getInt("idorganisateur"));

                //récupération des membres par appel au DAO gérant les Jury
                lstAnimators = DAOAnimator.getInstance().getAllByIdHackathon(hacka.getId());
                lstJury = DAOJury.getInstance().getAllByIdHackathon(hacka.getId());
                lstExpert = DAOExpert.getInstance().getAllByIdHackathon(hacka.getId());

                hacka.setAnimatorMembers(lstAnimators);
                hacka.setJuryMembers(lstJury);
                hacka.setExpertMembers(lstExpert);

                lstHackathon.add(hacka);
            }
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return lstHackathon;
    }

    /**
     * permet de recupérer toutes les room dispo pour un hackathon
     * @return ArrayList<String>
     * @throws DALException
     */
    public ArrayList<String> getRoom() throws DALException
    {
        ArrayList<String> roomString = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectRoomState);

            while(rs.next())
            {
                roomString.add(rs.getString("libelle"));
            }
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return roomString;
    }

    /**
     * Permet de récupérer un hackathon à partir de son id
     * @param id du hackathon
     * @return le hackathon dont l'id est transmis en paramètre
     * @throws DALException
     */
    @Override
    public Hackathon getOneById(int id) throws DALException
    {
        Hackathon  hackat=null;

        List<Member> lstMembers;
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);

            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                hackat = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"), rs.getString("place"), rs.getString("target"),rs.getString("type"),rs.getInt("nbrParticipants"), rs.getDate("date"), rs.getInt("idorganisateur"));

                lstMembers = DAOJury.getInstance().getAllByIdHackathon(hackat.getId());
                hackat.setJuryMembers(lstMembers);
            }
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
        return hackat;
    }

    /**
     * permet de recuperer l'id de l'invitation par son libelle
     * @param room
     * @return int
     * @throws DALException
     */
    private int getIdInvitationByString(String room) throws DALException
    {
        int id = 0;

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByType);
            rqt.setString(1, room);
            ResultSet rs = rqt.executeQuery();
            while (rs.next())
            {
                id = rs.getInt("id");
            }
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
        return id ;
    }


    /**
     * Permet l'insertion d'un nouvel hackathon
     * @param obj : hackathon à inserer
     * @throws DALException
     */
    public void saveSettings(Hackathon obj) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne, Statement.RETURN_GENERATED_KEYS);
            rqt.setString(1,obj.getName());
            rqt.setDate(2, new java.sql.Date(obj.getDate().getTime()));
            rqt.setString(3, obj.getTopic());
            rqt.setString(4, obj.getDescription());
            rqt.setString(5, obj.getPlace());
            rqt.setString(6, obj.getTarget());
            rqt.setInt(7, getIdInvitationByString(obj.getRoom()));
            rqt.setInt(8,obj.getNbrParticipants());
            rqt.setInt(9,obj.getIdOrganisateur());
            rqt.executeUpdate();
            rqt.getGeneratedKeys().next();
            obj.setId(rqt.getGeneratedKeys().getInt(1));
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    

    /**
     * Permet de mettre à jour la composition du jury du hackathon
     * @param idHackathon : hackathon auquel le jury participe
     * @throws DALException
     */
    public void saveJury(int idHackathon, Member oneJury) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertJuryCompose);
            rqt.setInt(1, oneJury.getId());
            rqt.setInt(2, idHackathon);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }


    public void saveAnimator(int idHackathon, Member oneAnimator) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertAnimateurCompose);
            rqt.setInt(1, oneAnimator.getId());
            rqt.setInt(2, idHackathon);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de mettre à jour la composition du jury du hackathon
     * @param idHackathon : hackathon auquel le jury participe
     * @throws DALException
     */
    public void saveExpert(int idHackathon, Expert oneMember) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertExpertCompose);
            rqt.setInt(1, oneMember.getId());
            rqt.setInt(2, idHackathon);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de mettre à jour uniquement les settings du hackathon dans la base de données
     * @param obj : hackathon à mettre à jour
     * @throws DALException
     */
    public void updateSettings(Hackathon obj) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();

            PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
            rqt.setString(1, obj.getName());
            rqt.setDate(2, new java.sql.Date(obj.getDate().getTime()));
            rqt.setString(3, obj.getTopic());
            rqt.setString(4, obj.getDescription());
            rqt.setString(5, obj.getPlace());
            rqt.setString(6, obj.getTarget());
            rqt.setInt(7, getIdInvitationByString(obj.getRoom()));
            rqt.setInt(8,obj.getNbrParticipants());
            rqt.setInt(9, obj.getIdOrganisateur());
            rqt.setInt(10, obj.getId());
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * permet de supprimer un hackaton par son id
     * @param id
     * @throws DALException
     */
    public void deleteHackathonById(int id) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteHackathonById);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * permet de supprimer toutes les inscription liées a un hackathon par id de l'hackathon
     * @param id
     * @throws DALException
     */
    public void deleteInscription(int id) throws DALException {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteInscription);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * permet de supprimer tout les projets lié a un hackathon
     * @param id
     * @throws DALException
     */
    public void deleteProjet(int id) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteprojet);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }


    @Override
    public void save(Hackathon obj) throws DALException {

    }

    @Override
    public void update(Hackathon obj) throws DALException {

    }
}
