package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOJury extends DAO<Member>
{
    private static DAOJury instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static synchronized DAOJury getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new DAOJury();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectIdByEmail = "select jury.id from jury inner join membre on jury.id = membre.id inner join \"user\" on membre.id = \"user\".idmembre where membre.email = ?";
    private static final String sqlSelectAllByIdHackathon = "select jury.id, lastname, firstname, phone, email from jurycompose inner join jury on jurycompose.idjury = jury.id inner join membre on jury.id = membre.id inner join \"user\" on membre.id = \"user\".idmembre where jurycompose.idhackathon=?";
    private static final String sqlSelectAllJury = "select jury.id, lastname, firstname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join jury on jury.id = membre.id";
    private static final String sqlSelectJuryWithout = "select jury.id, lastname, firstname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join jury on jury.id = membre.id where jury.id not in (select id from jury inner join jurycompose on jury.id = jurycompose.idjury where jurycompose.idhackathon = ?)";
    private static final String sqlInsertNewJury = "insert into jury(id) values(?)";



    /**
     * Permet de récupérer tous les jury
     * @return une liste des jury
     * @throws DALException
     */
    @Override
    public List<Member> getAll() throws DALException
    {
        List<Member> juries = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllJury);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member jury = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"),rs.getString("email"));
                juries.add(jury);
            }

            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return juries;
    }

    /**
     * Permet de récupérer tous les membres participant au jury du hackathon dont l'id est passé en paramètre
     * @return la liste des membres de jurys
     * @throws DALException
     */
    public List<Member> getAllByIdHackathon(int idHackathon) throws DALException
    {
        List<Member> juries = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllByIdHackathon);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member jury = new Member(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"),rs.getString("phone"), rs.getString("email"));
                juries.add(jury);
            }

            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return juries;
    }

    /**
     * permet de recuperer tout les jury ne participant pas a un hackathon
     * @param idHackathon
     * @return
     * @throws DALException
     */
    public List<Member> getJuryWithout(int idHackathon) throws DALException
    {
        List<Member> juries = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectJuryWithout);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member jury = new Member(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"),rs.getString("email"));
                juries.add(jury);
            }

            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return juries;
    }

    @Override
    public void save(Member obj) throws DALException
    {
        try
        {
            DAOMembre.getInstance().save(obj);

            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertNewJury);
            rqt.setInt(1, obj.getId());
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    @Override
    public void update(Member obj) throws DALException {

        DAOMembre.getInstance().update(obj);
    }

    @Override
    public Member getOneById(int id) throws DALException
    {
        return null;
    }

}
