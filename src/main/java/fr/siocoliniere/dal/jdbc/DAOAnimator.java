package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOAnimator extends DAO<Member>
{
    private static DAOAnimator instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static  synchronized DAOAnimator getInstance()
    {
        if(instanceCtrl == null)
        {
            instanceCtrl = new DAOAnimator();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectALLAnimator = "select animateur.id, lastname, firstname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join animateur on animateur.id = membre.id";
    private static final String sqlSelectAnimatorWithout = "select animateur.id, firstname, lastname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join animateur on animateur.id = membre.id where animateur.id not in (select id from animateur inner join animateurcompose on animateur.id = animateurcompose.idanimateur where animateurcompose.idhackathon = ?)";
    private static final String sqlSelectIdByEmail = "select animateur.id from membre inner join \"user\" on membre.id = \"user\".idmembre inner join animateur on animateur.id = membre.id where user.email = ?";
    private static final String sqlSelectAllByIdHackathon = "select animateur.id, lastname, firstname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join animateur on animateur.id = membre.id inner join animateurcompose on animateur.id = animateurcompose.idanimateur where idhackathon = ?";
    private static final String sqlInsertNewAnimator = "insert into animateur(id) values(?)";
    private static final String sqlUpdateOne = "update animateur set lastname = ?, firstname = ?, phone = ? , email = ? where id = ?";

    /**
     * permet de recuperer tout les animateurs en base de donnée
     * @return List<Member>
     * @throws DALException
     */
    public List<Member> getAll() throws DALException
    {
        List<Member> animators = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectALLAnimator);
            ResultSet rs = rqt.executeQuery();

            while(rs.next())
            {
                Member animator = new Member(rs.getInt("id"), rs.getString("lastname"),rs.getString("firstname"),rs.getString("phone"),rs.getString("email"));
                animators.add(animator);
            }
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return animators;
    }

    /**
     * permet de recuperer tout les animateurs qui ne sont pas dans un hackathon en basse de donnée
     * @param idHackathon
     * @return List<Member>
     * @throws DALException
     */
    public List<Member> getAnimatorWithout(int idHackathon) throws DALException
    {
        List<Member> animators = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAnimatorWithout);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member animator = new Member(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"),rs.getString("phone"),rs.getString("email"));
                animators.add(animator);
            }
            rqt.close();

        }
        catch (SQLException e)
        {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return animators;
    }

    /**
     * permet de recuperer tout les ainmateurs appartenant a un hackathon
     * @param idHackathon
     * @return List<Member>
     * @throws DALException
     */
    public List<Member> getAllByIdHackathon(int idHackathon) throws DALException
    {
        List<Member> animators = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllByIdHackathon);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member animator = new Member(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"),rs.getString("phone"), rs.getString("email"));
                animators.add(animator);
            }

        }
        catch (SQLException e)
        {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return animators;
    }

    /**
     * permet de sauvegarder un nouveau animateur
     * @param obj
     * @throws DALException
     */
    public void saveSettings(Member obj) throws DALException {
        try
        {
            DAOMembre.getInstance().save(obj);

            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertNewAnimator);
            rqt.setInt(1, obj.getId());
            rqt.executeUpdate();
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * met à jour les informations d'un animateur
     * @param obj
     * @throws DALException
     */
    public void updateSettings(Member obj) throws DALException
    {
        DAOMembre.getInstance().update(obj);
    }

    @Override
    public Member getOneById(int id) throws DALException {
        return null;
    }

    @Override
    public void save(Member obj) throws DALException {

    }

    @Override
    public void update(Member obj) throws DALException {

    }


}
