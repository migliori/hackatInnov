package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOOrganisateur extends DAO<Member>
{
    private static DAOOrganisateur instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static synchronized DAOOrganisateur getInstance()
    {
        if (instanceCtrl == null)
        {
            instanceCtrl = new DAOOrganisateur();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static final String sqlInsertNewOrganisateur = "insert into organisateur(id) values(?)";
    private static final String sqlSelectAllOrganisateur = "select organisateur.id, lastname, firstname, phone, email from membre inner join \"user\" on membre.id = \"user\".idmembre inner join organisateur on organisateur.id = membre.id";

    @Override
    public List<Member> getAll() throws DALException
    {
        List<Member> organisateurs = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllOrganisateur);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Member organisateur = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"),rs.getString("email"));
                organisateurs.add(organisateur);
            }

            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return organisateurs;
    }

    @Override
    public void save(Member obj) throws DALException
    {
        try
        {
            DAOMembre.getInstance().save(obj);

            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertNewOrganisateur);
            rqt.setInt(1, obj.getId());
            rqt.executeUpdate();
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
    }

    @Override
    public void update(Member obj) throws DALException
    {
        DAOMembre.getInstance().update(obj);
    }

    @Override
    public Member getOneById(int id) throws DALException
    {
        return null;
    }
}
