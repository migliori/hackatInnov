package fr.siocoliniere.dal.jdbc;


import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.List;

public class DAOMembre extends DAO<Member>
{
    private static DAOMembre instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static synchronized DAOMembre getInstance()
    {
        if (instanceCtrl == null)
        {
            instanceCtrl = new DAOMembre();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static  final String sqlInsertOne = "insert into membre(lastname, firstname, phone, email, password) values (?,?,?,?,?)";
    private static final String sqlUpdateMember = "select updatemembreuser(?, ?, ?, ?, ?, ?)";
    private static final String sqlGetEmailIsExist = "select idmembre from \"user\" where email = ?";
    private static final String sqlCreateMemberProcedure = "select createMembreUser(?, ?, ?, ?, null)";
    private static final String sqlGetPasswordHash = "select password from \"user\" where idmembre = ?";

    @Override
    public List<Member> getAll() throws DALException
    {
        return null;
    }

    @Override
    public Member getOneById(int id) throws DALException
    {
        return null;
    }

    @Override
    public void save(Member obj) throws DALException
    {
        int idNewMember = 0;
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlCreateMemberProcedure);
            rqt.setString(1, obj.getLastname());
            rqt.setString(2, obj.getFirstname());
            if(obj.getPhone().equals(""))
            {
                rqt.setNull(3, Types.VARCHAR);
            }
            else
            {
                rqt.setString(3, obj.getPhone());
            }
            rqt.setString(4, obj.getEmail());
            ResultSet rs = rqt.executeQuery();
            while(rs.next())
            {
                idNewMember = rs.getInt("createmembreuser");
            }
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        obj.setId(idNewMember);
    }


    @Override
    public void update(Member obj) throws DALException
    {
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlUpdateMember);
            rqt.setInt(1, obj.getId());
            rqt.setString(2, obj.getLastname());
            rqt.setString(3, obj.getFirstname());
            if(obj.getPhone().equals(""))
            {
                rqt.setNull(4, Types.VARCHAR);
            }
            else
            {
                rqt.setString(4, obj.getPhone());
            }
            rqt.setString(5, obj.getEmail());
            rqt.setString(6, getPassHashMember(obj.getId()));
            rqt.executeQuery();
            rqt.close();
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * permet de recuperer le mot de passe hashé de l'utilisateur
     * @param id
     * @return
     * @throws DALException
     */
    public String getPassHashMember(int id) throws DALException
    {
        String password = null;

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlGetPasswordHash);
            rqt.setInt(1, id);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                password = rs.getString("password");
            }

            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return password;
    }

    /**
     * permet de verifier si l'email saisie existe deja
     * @param email
     * @return
     * @throws DALException
     */
    public boolean emailExist(String email) throws DALException
    {
        boolean res;
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlGetEmailIsExist);
            rqt.setString(1, email);
            ResultSet rs = rqt.executeQuery();
            int nbCountWithEmail = 0;
            while(rs.next())
            {
                ++nbCountWithEmail;
            }
            res = nbCountWithEmail > 0;
            rqt.close();
        }

        catch (SQLException e){
            throw new DALException(e.getMessage(),e);
        }
        return res;
    }

    /**
     * permet de verifier si l'email saisie existe deja
     * @param membre
     * @param email
     * @return
     * @throws DALException
     */
    public boolean emailExist(Member membre, String email) throws DALException
    {
        boolean res;
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlGetEmailIsExist);
            rqt.setString(1, email);
            ResultSet rs = rqt.executeQuery();
            int nbCountWithEmail = 0;
            while(rs.next())
            {
                if(membre.getId() != rs.getInt("idmembre"))
                {
                    ++nbCountWithEmail;
                }
            }
            res = nbCountWithEmail > 0;
            rqt.close();
        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }
        return res;
    }
}
