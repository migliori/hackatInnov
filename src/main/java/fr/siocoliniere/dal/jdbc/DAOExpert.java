package fr.siocoliniere.dal.jdbc;


import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOExpert extends DAO<Expert>
{
    private static DAOExpert instanceCtrl;

    /**
     * getter sur l'attribut static instanceCtrl
     * @return DAOAnimator
     */
    public static synchronized DAOExpert getInstance()
    {
        if (instanceCtrl == null)
        {
            instanceCtrl = new DAOExpert();
        }
        return instanceCtrl;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectAllExperts = "select expert.id, lastname, firstname, phone, email,job from membre inner join \"user\" on membre.id = \"user\".idmembre inner join expert on expert.id = membre.id";
    private static final String sqlSelectIdByEmail = "select expert.id from expert inner join membre on expert.id = membre.id inner join \"user\"ser on membre.id = \"user\".idmembre where membre.email = ?";
    private static final String sqlSelectAllByIdHackathon = "select expert.id, lastname, firstname, phone, email,job from expertcompose inner join expert on expertcompose.idexpert = expert.id inner join membre on expert.id = membre.id inner join \"user\" on membre.id = \"user\".idmembre where expertcompose.idhackathon=?";
    private static final String sqlSelectExpertWithout = "select expert.id, lastname, firstname, phone, email,job from expert inner join membre on expert.id = membre.id inner join \"user\" on membre.id = \"user\".idmembre where expert.id not in (select id from expert inner join expertcompose on expert.id = expertcompose.idexpert where expertcompose.idhackathon = ?)";
    private static final String sqlInsertNewExpert = "insert into expert(id, job) values(?,?)";
    private static final String sqlUpdateExpert = "update expert SET job = ? WHERE id = ?";

    /**
     * permet de recuperer tout les experts en base de donnée
     * @return List<Member>
     * @throws DALException
     */
    @Override
    public List<Expert> getAll() throws DALException
    {
        List<Expert> experts = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllExperts);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Expert expert = new Expert(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"), rs.getString("email"), rs.getString("job"));
                experts.add(expert);
            }

            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return experts;
    }

    /**
     * permet de recuperer l'id d'un expert via son email
     * @param email
     * @return int
     * @throws DALException
     */
    public int getIdByEmail(String email) throws DALException
    {
        int id = 0;
        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByEmail);
            rqt.setString(1, email);
            ResultSet rs = rqt.executeQuery();
            while(rs.next())
            {
                id = rs.getInt("id");
            }
            rqt.close();

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return id;
    }

    /**
     * permet de recuperer tout les expert lié a un hackathon
     * @param idHackathon
     * @return List<Expert>
     * @throws DALException
     */
    public List<Expert> getAllByIdHackathon(int idHackathon) throws DALException
    {
        List<Expert> experts = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllByIdHackathon);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Expert member = new Expert(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"), rs.getString("email"), rs.getString("job"));
                experts.add(member);
            }

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return experts;
    }

    /**
     * permet de recuperer tout les experts qui ne sont pas dans un hackathon
     * @param idHackathon
     * @return
     * @throws DALException
     */
    public List<Expert> getExpertWithout(int idHackathon) throws DALException
    {
        List<Expert> experts = new ArrayList<>();

        try
        {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectExpertWithout);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next())
            {
                Expert member = new Expert(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"), rs.getString("phone"), rs.getString("email"), rs.getString("job"));
                experts.add(member);
            }

        }
        catch (SQLException e)
        {
            throw new DALException(e.getMessage(),e);
        }

        return experts;
    }

    @Override
    public Expert getOneById(int id) throws DALException
    {
        return null;
    }

    @Override
    public void save(Expert obj) throws DALException
    {
        try
        {
            DAOMembre.getInstance().save(obj);

            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertNewExpert);
            rqt.setInt(1, obj.getId());
            rqt.setString(2, obj.getJob());
            rqt.executeUpdate();
            rqt.close();
        }
        catch(SQLException e)
        {
            obj.setId(this.getIdByEmail(obj.getEmail()));
            e.printStackTrace();
        }

    }

    @Override
    public void update(Expert obj) throws DALException
    {
        try
        {
            DAOMembre.getInstance().update(obj);

            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlUpdateExpert);
            rqt.setString(1, obj.getJob());
            rqt.setInt(2, obj.getId());
            rqt.executeUpdate();
            rqt.close();
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
}
