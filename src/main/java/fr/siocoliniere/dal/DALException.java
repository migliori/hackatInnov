package fr.siocoliniere.dal;

public class DALException extends Exception
{
	/**
	 * Constructeur de la classe Exception
	 */
	public DALException()
	{
		super();
	}
	
	public DALException(String message)
	{
		super(message);
	}
	
	public DALException(String message, Throwable exception)
	{
		super(message, exception);
	}


	@Override
	public String getMessage()
	{
		return "Couche DAL - " + super.getMessage();

	}
	
	
}
