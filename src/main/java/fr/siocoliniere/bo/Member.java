package fr.siocoliniere.bo;

import java.util.Objects;

public class Member
{
    private int id;
    private String lastname;
    private String firstname;
    private String phone;
    private String email;

    /**
     * Constructeur de la classe Member
     * @param lastname nom du membre
     * @param firstname prenom du membre
     */
    public Member(String lastname, String firstname, String phone, String email)
    {
        this.id = 0;
        this.lastname = lastname;
        this.firstname = firstname;
        this.phone = phone;
        this.email = email;
    }

    /**
     * constructeur de la classee Member
     * @param id
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     */
    public Member(int id, String lastname, String firstname, String phone, String email)
    {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.phone = phone;
        this.email = email;
    }


    /**
     * Getter sur l'attribut id
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Getter sur l'attribut lastname
     * @return String
     */
    public String getLastname()
    {
        return lastname;
    }

    /**
     * Getter sur l'attribut firstname
     * @return String
     */
    public String getFirstname()
    {
        return firstname;
    }

    /**
     * Getter sur l'attribut phone
     * @return String
     */
    public String getPhone()
    {
        return phone;
    }

    /**
     * Getter sur l'attribut email
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Setter sur l'attribut lastname
     * @param lastname
     */
    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    /**
     * Setter sur l'attribut firstname
     * @param firstname
     */
    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    /**
     * Setter sur l'attribut phone
     * @param phone
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    /**
     * Setter sur l'attribut email
     * @param email
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * Setter sur l'attribut id
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return  lastname + ' ' + firstname ;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return id == member.id && Objects.equals(email, member.email);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, email);
    }
}
