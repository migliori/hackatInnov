package fr.siocoliniere.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Hackathon
{
    private int id;
    private String name;
    private String topic;
    private Date date;
    private String description;
    private String place;
    private String target;
    private String room;
    private Integer nbrParticipants;
    private int idOrganisateur;

    private List<Member> animatorMembers;
    private List<Member> juryMembers; // liste des membres du jury du hackathon
    private List<Expert> expertMembers;

    /**
     * Constructeur de la classe hackathon
     * @param topic topic du hackathon
     * @param desc description du hackathon
     * @param place lieu du hackathon
     * @param target objectif de l'hackathon
     */
    public Hackathon(String name,String topic, String desc, String place, String target, String room, Integer nbrParticipants, Date date, int idOrganisateur)
    {
        this.id = 0;
        this.name = name;
        this.topic = topic;
        this.description = desc;
        this.place = place;
        this.target = target;
        this.room = room;
        this.date = date;
        this.nbrParticipants = nbrParticipants;
        this.idOrganisateur = idOrganisateur;

        this.animatorMembers = new ArrayList<>();
        this.juryMembers = new ArrayList<>();
        this.expertMembers = new ArrayList<>();

    }

    /**
     * constructeur de la classe hackathon
     * @param id
     * @param name
     * @param topic
     * @param desc
     * @param place
     * @param target
     * @param room
     * @param nbrParticipants
     * @param date
     * @param idOrganisateur
     */
    public Hackathon(int id, String name, String topic, String desc, String place, String target, String room, Integer nbrParticipants, Date date, int idOrganisateur)
    {
        this(name, topic, desc, place, target, room, nbrParticipants, date, idOrganisateur);
        this.id = id;

    }

    /**
     * Getter sur l'attribut id
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Getter sur l'attribut name
     * @return String
     */
    public String getName()
    {
        return name;
    }

    /**
     * Getter sur l'attribut topic
     * @return String
     */
    public String getTopic()
    {
        return topic;
    }

    /**
     * Getter sur l'attribut description
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Getter sur l'attribut place
     * @return String
     */
    public String getPlace()
    {
        return place;
    }

    /**
     * Getter sur l'attribut target
     * @return String
     */
    public String getTarget()
    {
        return target;
    }

    /**
     * Getter sur l'attribut room
     * @return String
     */
    public String getRoom()
    {
        return room;
    }

    /**
     * Getter sur l'attribut date
     * @return Date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * Getter sur l'attribut nbrParticipants
     * @return int
     */
    public Integer getNbrParticipants()
    {
        return nbrParticipants;
    }

    /**
     * Getter sur l'attribut juryMember
     * @return List<Member>
     */
    public List<Member> getJuryMembers()
    {
        return juryMembers;
    }

    /**
     * Getter sur l'attribut animatorMembers
     * @return List<Member>
     */
    public List<Member> getAnimatorMembers()
    {
        return animatorMembers;
    }

    /**
     * Getter sur l'attribut expertsMembers
     * @return List<Expert>
     */
    public List<Expert> getExpertMembers()
    {
        return expertMembers;
    }

    /**
     * Getter sur l'attribut idOrganisateur
     * @return int
     */
    public int getIdOrganisateur()
    {
        return idOrganisateur;
    }

    /**
     * Setter sur l'attibut id
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Setter sur l'attibut name
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Setter sur l'attibut topic
     * @param topic
     */
    public void setTopic(String topic)
    {
        this.topic = topic;
    }

    /**
     * Setter sur l'attibut description
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Setter sur l'attibut date
     * @param date
     */
    public void setDate(Date date)
    {
        this.date = date;
    }

    /**
     * Setter sur l'attibut place
     * @param place
     */
    public void setPlace(String place)
    {
        this.place = place;
    }

    /**
     * Setter sur l'attibut target
     * @param target
     */
    public void setTarget(String target)
    {
        this.target = target;
    }

    /**
     * Setter sur l'attibut room
     * @param room
     */
    public void setRoom(String room)
    {
        this.room = room;
    }

    /**
     * Setter sur l'attibut nbrParticipants
     * @param nbrParticipants
     */
    public void setNbrParticipants(Integer nbrParticipants)
    {
        this.nbrParticipants = nbrParticipants;
    }

    /**
     * Setter sur l'attibut animatorMembers
     * @param animatorMembers
     */
    public void setAnimatorMembers(List<Member> animatorMembers)
    {
        this.animatorMembers = animatorMembers;
    }

    /**
     * Setter sur l'attibut juryMembers
     * @param memberMembers
     */
    public void setJuryMembers(List<Member> memberMembers)
    {
        this.juryMembers = memberMembers;
    }

    /**
     * Setter sur l'attibut expertMembers
     * @param memberMembers
     */
    public void setExpertMembers(List<Expert> memberMembers)
    {
        this.expertMembers = memberMembers;
    }

    /**
     * Setter sur l'attibut idOrganisateur
     * @param id
     */
    public void setIdOrganisateur(int id)
    {
        this.idOrganisateur = id;
    }


    /**
     * Permet l'ajout d'un membre au animateur du hackathon
     * @param member nouveau membre
     */
    public void addAnimatorMember(Member member)
    {
        animatorMembers.add(member);
    }

    /**
     * Permet l'ajout d'un membre au jury du hackathon
     * @param member
     */
    public void addJuryMember(Member member)
    {
        juryMembers.add(member);

    }

    /**
     * Permet l'ajout d'un membre au expert du hackathon
     * @param member
     */
    public void addExpertMember(Expert member){expertMembers.add(member);}

    /**
     * Permet de vider la liste de membres composant le jury
     */
    public void clearJury()
    {
        this.juryMembers.clear();
    }

    /**
     * Permet de vider la liste de membres composant les animateurs
     */
    public void clearAnimator()
    {
        this.animatorMembers.clear();
    }

    /**
     * Permet de vider la liste de membres composant les experts
     */
    public void clearExpert()
    {
        this.expertMembers.clear();
    }

    @Override
    public String toString()
    {
        return this.name + " - " + this.topic;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hackathon hackathon = (Hackathon) o;
        return id == hackathon.id && Objects.equals(name, hackathon.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name);
    }
}
