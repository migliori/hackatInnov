package fr.siocoliniere.bo;

public class Expert extends Member
{
    private String job;

    /**
     * constructeur de la classe Expert
     * @param id
     * @param lastname  nom du membre
     * @param firstname prenom du membre
     */
    public Expert(int id, String lastname, String firstname, String phone, String email, String job)
    {
        super(id, lastname, firstname, phone, email);
        this.job = job;
    }

    /**
     * constructeur de la classe Expert
     * @param lastname
     * @param firstname
     * @param phone
     * @param email
     * @param job
     */
    public Expert(String lastname, String firstname, String phone, String email, String job)
    {
        super(0, lastname, firstname, phone, email);
        this.job = job;
    }

    /**
     * Getter sur l'attribut job
     * @return String
     */
    public String getJob()
    {
        return this.job;
    }

    /**
     * Setter sur l'attribut job
     * @param job
     */
    public void setJob(String job)
    {
        this.job = job;
    }

    @Override
    public String toString()
    {
        return super.toString() + " - " + this.job ;
    }

    @Override
    public boolean equals(Object o)
    {
        return super.equals(o);
    }
}
