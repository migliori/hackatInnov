package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.border.*;

import fr.siocoliniere.bll.*;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.bo.Expert;

public class MemberForm extends JDialog
{
    private Member memberManaged;
    private WelcomeForm welcome;
    private String indicator;

    /**
     * Constructeur de la classe MemberForm
     */
    public MemberForm()
    {
        this.memberManaged = null;

        // window settings
        setPreferredSize(new Dimension(400, 400));
        setLocation(500, 500);
        setModal(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        initComponents();
    }

    /**
     * initialise les textfield et le titre de la view
     * @param memberManaged
     * @param indicator
     * @param welcome
     */
    public void initManaged(Member memberManaged, String indicator, WelcomeForm welcome)
    {
        initTextLanguage();

        this.memberManaged = memberManaged;
        this.welcome = welcome;

        this.phoneTextField.setBorder(BorderFactory.createLineBorder(Color.black));
        this.indicator = indicator;

        String lastName = "";
        String firstName = "";
        String phone = "";
        String email = "";
        String job = "";

        if(memberManaged != null)
        {
            setTitle("Hackat'Orga : " + this.memberManaged.toString());

            lastName = memberManaged.getLastname();
            firstName = memberManaged.getFirstname();
            phone = memberManaged.getPhone();
            email = memberManaged.getEmail();

            if(indicator.equalsIgnoreCase("Expert"))
            {
                job = ((Expert) memberManaged).getJob();
            }
        }
        else
        {
            setTitle("Hackat'Orga : " + GlobalController.getInstance().getProperty(indicator + "Btn"));
        }

        lastnameTextField.setText(lastName);
        firstnameTextField.setText(firstName);
        phoneTextField.setText(phone);
        emailTextField.setText(email);
        jobTextField.setText(job);

        clearBorderTextField();

        errorFormIndication.setText(GlobalController.getInstance().getProperty("needTextField"));
        errorFormIndication.setForeground(Color.black);
    }

    /**
     * Permet de fermer la page
     * Appelée : lors du clic sur Cancel
     * @param e
     */
    private void cancelButtonActionPerformed(ActionEvent e)
    {
        this.dispose();
    }

    /**
     * permet de clear les border de tout les textfield de la vue
     */
    private void clearBorderTextField()
    {
        JTextField[] textFields = {lastnameTextField, firstnameTextField, phoneTextField, emailTextField, jobTextField};
        for(JTextField textField : textFields)
        {
            textField.setBorder(BorderFactory.createLineBorder(Color.black));
        }
    }

    /**
     * permet de traiter les données afin de sauverger (save(): void)
     * Appelée: lors du clic sur Save
     * @param e
     */
    private void saveButtonActionPerformed(ActionEvent e)
    {
        clearBorderTextField();
        errorFormIndication.setForeground(Color.black);

        Pattern mailPattern = Pattern.compile("^.+@.+\\..+");
        Matcher mailSaisie = mailPattern.matcher(emailTextField.getText());

        String lastName = lastnameTextField.getText().trim();
        String firstName = firstnameTextField.getText().trim();
        String email = emailTextField.getText().trim();
        String phone = phoneTextField.getText().trim();
        String job = jobTextField.getText().trim();

        if(lastName.equals("") || firstName.equals("") || email.equals("") || !(mailSaisie.matches()))
        {
            if(lastName.equals(""))
            {
                lastnameTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(firstName.equals(""))
            {
                firstnameTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(email.equals("") || !(mailSaisie.matches()))
            {
                emailTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            GlobalController.getInstance().getProperty("needTextField");
        }
        else
        {
            if(indicator.equals("Expert") && job.equals(""))
            {
                jobTextField.setBorder(BorderFactory.createLineBorder(Color.red));
                GlobalController.getInstance().getProperty("needTextField");
            }
            else
            {
                if(!phone.equals("") && !GlobalController.getInstance().tryParseInt(phone))
                {
                    phoneTextField.setBorder(BorderFactory.createLineBorder(Color.red));
                    GlobalController.getInstance().getProperty("phoneExist");
                }
                else
                {
                    if(memberManaged == null)
                    {
                        if(MemberController.getInstance().hasSameEmailInList(email))
                        {
                            emailTextField.setBorder(BorderFactory.createLineBorder(Color.red));
                            GlobalController.getInstance().getProperty("emailExist");
                        }
                        else
                        {
                            save(lastName, firstName, phone, email, job);
                        }
                    }
                    else
                    {
                        if(MemberController.getInstance().hasSameEmailInList(memberManaged, email))
                        {
                            emailTextField.setBorder(BorderFactory.createLineBorder(Color.red));
                            GlobalController.getInstance().getProperty("emailExist");
                        }
                        else
                        {
                            save(lastName, firstName, phone, email, job);
                        }
                    }
                }
            }
        }
    }

    /**
     * permet de save les données a partir de l'indicateur
     */
    private void save(String lastName, String firstName, String phone, String email, String job)
    {
        if(indicator.equals("Jury"))
        {
            Member newJuryManaged = JuryController.getInstance().saveJuryStandalone(memberManaged, lastName, firstName, phone, email);
            welcome.addJuryComboBox(newJuryManaged);
        }
        else if(indicator.equals("Animator"))
        {
            Member newAnimateurManaged = AnimatorController.getInstance().saveAnimatorStandalone(memberManaged, lastName, firstName, phone, email);
            welcome.addAnimatorComboBox(newAnimateurManaged);
        }
        else if(indicator.equals("Expert"))
        {
            Expert newExpertManaged = ExpertController.getInstance().saveExpertStandalone((Expert) memberManaged, lastName, firstName, phone, email, job);
            welcome.addExpertInExpertComboBox(newExpertManaged);
        }
        else
        {
            Member newOrganisateurManaged = OrganisateurController.getInstance().saveOrganisateurStandalone(memberManaged, lastName, firstName, phone, email);
            welcome.addOrganisateurComboBox(newOrganisateurManaged);
        }

        this.dispose();
    }

    /**
     * permet d'initialiser les champ de texte au language choisit (default: englais)
     */
    private void initTextLanguage()
    {
        lastnameLabel.setText(GlobalController.getInstance().getProperty("lastNameLabel"));
        firstnameLabel.setText(GlobalController.getInstance().getProperty("firstNameLabel"));
        phoneLabel.setText(GlobalController.getInstance().getProperty("phoneLabel"));
        emailLabel.setText(GlobalController.getInstance().getProperty("emailPhone"));
        jobLabel.setText(GlobalController.getInstance().getProperty("jobLabel"));
        saveButton.setText(GlobalController.getInstance().getProperty("saveBtn"));
        cancelButton.setText(GlobalController.getInstance().getProperty("cancelBtn"));
    }

    /**
     * setter sur la visibilité des composant job
     */
    public void setVisibleJobPart(boolean state)
    {
        this.jobTextField.setVisible(state);
        this.jobLabel.setVisible(state);
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        lastnameLabel = new JLabel();
        lastnameTextField = new JTextField();
        vSpacer3 = new JPanel(null);
        firstnameLabel = new JLabel();
        firstnameTextField = new JTextField();
        vSpacer2 = new JPanel(null);
        phoneLabel = new JLabel();
        phoneTextField = new JTextField();
        vSpacer1 = new JPanel(null);
        emailLabel = new JLabel();
        emailTextField = new JTextField();
        vSpacer4 = new JPanel(null);
        jobLabel = new JLabel();
        jobTextField = new JTextField();
        buttonBar = new JPanel();
        errorFormIndication = new JLabel();
        saveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 25, 0, 26, 27, 26, 30, 27, 32, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- lastnameLabel ----
                lastnameLabel.setText("Lastname*");
                contentPanel.add(lastnameLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(lastnameTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                contentPanel.add(vSpacer3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- firstnameLabel ----
                firstnameLabel.setText("Firstname*");
                contentPanel.add(firstnameLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(firstnameTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- phoneLabel ----
                phoneLabel.setText("Phone");
                contentPanel.add(phoneLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(phoneTextField, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer1, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- emailLabel ----
                emailLabel.setText("Email*");
                contentPanel.add(emailLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(emailTextField, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer4, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- jobLabel ----
                jobLabel.setText("Job*");
                contentPanel.add(jobLabel, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(jobTextField, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 1.0, 0.0};

                //---- errorFormIndication ----
                errorFormIndication.setFont(errorFormIndication.getFont().deriveFont(errorFormIndication.getFont().getSize() - 1f));
                errorFormIndication.setForeground(Color.black);
                buttonBar.add(errorFormIndication, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- saveButton ----
                saveButton.setText("Save");
                saveButton.addActionListener(e -> saveButtonActionPerformed(e));
                buttonBar.add(saveButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel lastnameLabel;
    private JTextField lastnameTextField;
    private JPanel vSpacer3;
    private JLabel firstnameLabel;
    private JTextField firstnameTextField;
    private JPanel vSpacer2;
    private JLabel phoneLabel;
    private JTextField phoneTextField;
    private JPanel vSpacer1;
    private JLabel emailLabel;
    private JTextField emailTextField;
    private JPanel vSpacer4;
    private JLabel jobLabel;
    private JTextField jobTextField;
    private JPanel buttonBar;
    private JLabel errorFormIndication;
    private JButton saveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}