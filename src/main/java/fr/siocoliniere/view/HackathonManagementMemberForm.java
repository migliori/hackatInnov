package fr.siocoliniere.view;

import fr.siocoliniere.bll.*;
import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

public class HackathonManagementMemberForm extends JDialog
{

    private Hackathon hackathonManaged;
    private String indicator;

    /**
     * Constructeur de la classe HackathonManagementMemberForm
     * @param owner
     */
    public HackathonManagementMemberForm(Window owner)
    {
        super(owner);

        // window settings
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));


        initComponents();

        memberJList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                if (evt.getClickCount() == 2)
                {
                    removeMember();
                }
            }
        });

        potentialMemberJList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                if (evt.getClickCount() == 2) {

                    addMember();
                }
            }
        });

    }

    /**
     * Permet d'initialiser la vue pour un hacakthon choisi
     * @param hackathonManaged
     */
    public void initHackathonManaged(Hackathon hackathonManaged,String indicator){
        initTextLanguage();

        this.hackathonManaged = hackathonManaged;
        this.indicator = indicator;

        if(this.hackathonManaged != null)
        {
            setTitle("Hackat'Orga : " + GlobalController.getInstance().getProperty(indicator + "Btn"));
            if (indicator.equals("animator"))
            {
                initAnimatorList(hackathonManaged.getAnimatorMembers());
                initPotentialAnimatorList(AnimatorController.getInstance().getWithout(hackathonManaged));
            }
            else if(indicator.equals("expert"))
            {
                initExpertList(hackathonManaged.getExpertMembers());
                initExpertPotentialList(ExpertController.getInstance().getWithout(hackathonManaged));
            }
            else
            {
                initJuryList(hackathonManaged.getJuryMembers());
                initPotentialJuryList(JuryController.getInstance().getWithout(hackathonManaged));
            }
        }
    }

    /**
     * Permet de valoriser la Jlist des jury actuels du hackathon
     * @param memberList liste des membres du jury
     */
    private void initJuryList(List<Member> memberList)
    {
        DefaultListModel<Member> modelMemberList = new DefaultListModel<>();
        for (Member oneMember : memberList) {
            modelMemberList.addElement(oneMember);
        }
        this.memberJList.setModel(modelMemberList);

    }

    /**
     * Permet de valoriser la Jlist des animateurs actuels du hackathon
     * @param memberList liste des animateur
     */
    private void initAnimatorList(List<Member> memberList)
    {
        DefaultListModel<Member> modelMemberList = new DefaultListModel<>();

        for (Member oneAnimator : memberList)
        {
            modelMemberList.addElement(oneAnimator);
        }

        this.memberJList.setModel(modelMemberList);

    }

    /**
     * Permet de valoriser la Jlist des experts actuels du hackathon
     * @param expertList liste des expert
     */
    private void initExpertList(List<Expert> expertList)
    {
        DefaultListModel<Expert> modelExpertList = new DefaultListModel<>();
        for(Expert oneMember : expertList)
        {
            modelExpertList.addElement(oneMember);
        }

        this.memberJList.setModel(modelExpertList);
    }

    /**
     * Permet de valoriser la liste des jury potentiels
     * @param juries liste de jury
     */
    private void initPotentialJuryList(List<Member> juries)
    {
        DefaultListModel<Member> modelPotentialJuryList = new DefaultListModel<>();
        for (Member oneMember : juries)
        {
            modelPotentialJuryList.addElement(oneMember);
        }

        this.potentialMemberJList.setModel(modelPotentialJuryList);
    }

    /**
     * Permet de valoriser la liste des animateurs potentiels
     * @param animators liste d'animateur
     */
    private void initPotentialAnimatorList(List<Member> animators)
    {
        DefaultListModel<Member> modelPotentialAnimatorList = new DefaultListModel<>();
        for (Member oneMember : animators) {

            modelPotentialAnimatorList.addElement(oneMember);
        }

        this.potentialMemberJList.setModel(modelPotentialAnimatorList);
    }

    /**
     * Permet de valoriser la liste des experts potentiels
     * @param experts liste d'expert
     */
    private void initExpertPotentialList(List<Expert> experts)
    {
        DefaultListModel<Expert> modelPotentialJuryList = new DefaultListModel<>();
        for (Expert oneMember : experts)
        {
            modelPotentialJuryList.addElement(oneMember);
        }

        this.potentialMemberJList.setModel(modelPotentialJuryList);
    }

    /**
     * Permet de retirer le membre sélectionné de la liste du jury
     * Appelée : lors du clic sur le bouton Remove
     */
    private void removeButtonActionPerformed(ActionEvent e)
    {
        removeMember();
    }

    /**
     * permet de retirer le membre de la liste jury
     */
    private void removeMember()
    {
        //récupération du jury sélectionné
        Member memberSelected = (Member) memberJList.getSelectedValue();

        if(memberSelected != null)
        {
            //suppression du model rattaché à la Jlist gérant le jury
            DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) memberJList.getModel();
            modelJuryList.removeElement(memberSelected);

            //ajout au model rattacha à la JList gérant les jurys potentiels
            DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialMemberJList.getModel();
            modelMemberList.addElement(memberSelected);
        }
    }

    /**
     * Permet d'ajouter le membre sélectionné de la liste des membres
     * Appelée : lors du clic sur le bouton Add
     */
    private void addButtonActionPerformed(ActionEvent e)
    {
       addMember();
    }

    /**
     * Permet d'ajouter le membre sélectionné de la liste des membres
     */
    public void addMember()
    {
        Member potentialMemberSelected = (Member) potentialMemberJList.getSelectedValue();

        if(potentialMemberSelected != null)
        {
            DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) memberJList.getModel();
            modelJuryList.addElement(potentialMemberSelected);

            DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialMemberJList.getModel();
            modelMemberList.removeElement(potentialMemberSelected);
        }
    }

    /**
     * Permet de sauvegarder la liste de membre du hackathon
     * Appelée : lors du clic sur le bouton Save
     */
    private void SaveButtonActionPerformed(ActionEvent e)
    {
       if(this.indicator.equals("expert"))
       {
           List<Expert> experts = new ArrayList<>();

           for(int i = 0; i < memberJList.getModel().getSize(); i++)
           {
               Expert member = (Expert) memberJList.getModel().getElementAt(i);
               experts.add(member);
           }

           HackathonController.getInstance().saveExpertsByIdHackathon(hackathonManaged, experts);
       }
        else
       {
           List<Member> members = new ArrayList<>();

           for(int i = 0; i < memberJList.getModel().getSize(); i++)
           {
               Member member = (Member) memberJList.getModel().getElementAt(i);
               members.add(member);
           }

           if(this.indicator.equals("animator"))
           {
               HackathonController.getInstance().saveAnimatorsByIdHackathon(hackathonManaged, members);
           }
           else
           {
               HackathonController.getInstance().saveJuriesByIdHackathon(hackathonManaged,members);
           }
       }
       this.dispose();
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e)
    {
        this.dispose();
    }

    /**
     * permet d'initialiser les champ de texte au language choisit (default: englais)
     */
    private void initTextLanguage()
    {
        addButton.setText(GlobalController.getInstance().getProperty("addBtn"));
        removeButton.setText(GlobalController.getInstance().getProperty("removeBtn"));
        cancelButton.setText(GlobalController.getInstance().getProperty("cancelBtn"));
        SaveButton.setText(GlobalController.getInstance().getProperty("saveBtn"));
        potentialLabel.setText(GlobalController.getInstance().getProperty("potentialLabel"));
        existingLabel.setText(GlobalController.getInstance().getProperty("existLabel"));
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        existingLabel = new JLabel();
        potentialLabel = new JLabel();
        juryMemberScrollPane = new JScrollPane();
        memberJList = new JList();
        ARButtonPanel = new JPanel();
        addButton = new JButton();
        removeButton = new JButton();
        memberScrollPane = new JScrollPane();
        potentialMemberJList = new JList();
        buttonBar = new JPanel();
        SaveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 112, 0, 119, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};

                //---- existingLabel ----
                existingLabel.setText("Existing");
                contentPanel.add(existingLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- potentialLabel ----
                potentialLabel.setText("Potential");
                contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== juryMemberScrollPane ========
                {
                    juryMemberScrollPane.setViewportView(memberJList);
                }
                contentPanel.add(juryMemberScrollPane, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(5, 5, 5, 10), 0, 0));

                //======== ARButtonPanel ========
                {
                    ARButtonPanel.setLayout(new GridBagLayout());
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                    //---- addButton ----
                    addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
                    addButton.setText("Add");
                    addButton.addActionListener(e -> addButtonActionPerformed(e));
                    ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));

                    //---- removeButton ----
                    removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
                    removeButton.setText("Remove");
                    removeButton.addActionListener(e -> removeButtonActionPerformed(e));
                    ARButtonPanel.add(removeButton, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));
                }
                contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== memberScrollPane ========
                {
                    memberScrollPane.setViewportView(potentialMemberJList);
                }
                contentPanel.add(memberScrollPane, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- SaveButton ----
                SaveButton.setText("Save");
                SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
                buttonBar.add(SaveButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel existingLabel;
    private JLabel potentialLabel;
    private JScrollPane juryMemberScrollPane;
    private JList memberJList;
    private JPanel ARButtonPanel;
    private JButton addButton;
    private JButton removeButton;
    private JScrollPane memberScrollPane;
    private JList potentialMemberJList;
    private JPanel buttonBar;
    private JButton SaveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
