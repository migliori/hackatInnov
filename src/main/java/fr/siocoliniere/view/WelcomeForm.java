package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

import fr.siocoliniere.bll.*;
import fr.siocoliniere.bo.Expert;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import net.miginfocom.swing.*;

public class WelcomeForm extends JFrame
{
    private HackathonManagementForm viewHackathonManagement;
    private MemberForm viewMemberForm;
    private List<Hackathon> hackathons = HackathonController.getInstance().getAll();

    /**
     * Constructeur de la classe WelcomForm
     */
    public WelcomeForm()
    {
        // load english language for app
        GlobalController.getInstance().loadLanguageByName("english");

        // window settings
        setTitle("Hackat'Orga");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(600, 700));

        initComponents();

        // hide error label
        JLabel[] hideLabel = {errorLabel, errorJuryLabel, errorAnimatorLabel, errorExpertLabel, errorOrganisateurLabel};
        for(JLabel unErrorLabel : hideLabel)
        {
            unErrorLabel.setVisible(false);
        }

        // init all combo box
        initComboBoxHackathons();
        initComboBoxJuries();
        initComboBoxAnimator();
        initComboBoxExpert();
        initComboBoxOrganisateur();
        initComboBoxLangue();

        // create next view
        this.viewHackathonManagement = new HackathonManagementForm();
        this.viewHackathonManagement.setVisible(false);

        this.viewMemberForm = new MemberForm();
        this.viewMemberForm.setVisible(false);

        // init language
        initTextLanguage();
    }


    /**
     * Initialise items in hackathons combo box
     */
    private void initComboBoxHackathons()
    {
        if (hackathons != null && !hackathons.isEmpty())
        {
            DefaultComboBoxModel<Hackathon> modelCombo = new DefaultComboBoxModel<>();
            for (Hackathon unHackathon : hackathons)
            {
                modelCombo.addElement(unHackathon);
            }
            hackathonsComboBox.setModel(modelCombo);

        }
        else
        {
            this.errorLabel.setText("Aucun hackthons");
            this.errorLabel.setVisible(true);
            this.manageButton.setEnabled(false);
        }
    }

    /**
     * Initialise items in juries combo box
     */
    public void initComboBoxJuries()
    {
        List<Member> juries = JuryController.getInstance().getAll();

        if(juries != null && !juries.isEmpty())
        {
            DefaultComboBoxModel<Member> modelComboBox = new DefaultComboBoxModel<>();

            for(Member unJury : juries)
            {
                modelComboBox.addElement(unJury);
            }
            juriesComboBox.setModel(modelComboBox);
        }
        else
        {
            this.errorJuryLabel.setText("Aucun jury");
            this.errorJuryLabel.setVisible(true);
            this.manageJuryButton.setEnabled(false);
        }
    }

    /**
     * Initialise items in Animator combo box
     */
    public void initComboBoxAnimator()
    {
        List<Member> animateurs = AnimatorController.getInstance().getAll();

        if(animateurs != null && !animateurs.isEmpty())
        {
            DefaultComboBoxModel<Member> modelComboBox = new DefaultComboBoxModel<>();
            for(Member unAnimateur: animateurs)
            {
                modelComboBox.addElement(unAnimateur);
            }
            animatorsComboBox.setModel(modelComboBox);
        }
        else
        {
            this.errorAnimatorLabel.setText("Aucun animateurs");
            this.errorAnimatorLabel.setVisible(true);
            this.manageAnimatorButton.setEnabled(false);
        }
    }

    /**
     * Initialise items in organisateur combo box
     */
    public void initComboBoxOrganisateur()
    {
        List<Member> organisateurs = OrganisateurController.getInstance().getAll();

        if(organisateurs != null && !organisateurs.isEmpty())
        {
            DefaultComboBoxModel<Member> modelComboBox = new DefaultComboBoxModel<>();
            for(Member unOrganisateur: organisateurs)
            {
                modelComboBox.addElement(unOrganisateur);
            }
            organisateurComboBox.setModel(modelComboBox);
        }
        else
        {
            this.errorOrganisateurLabel.setText("Aucun Organisateur");
            this.errorOrganisateurLabel.setVisible(true);
            this.manageOrganisateurButton.setEnabled(false);
        }
    }

    /**
     * Initialise items in langue combo box
     */
    private void initComboBoxLangue()
    {
        List<String> langues = new ArrayList<>();
        langues.add("english");
        langues.add("francais");

        DefaultComboBoxModel<String> modelComboBox = new DefaultComboBoxModel<>();
        for(String lang : langues)
        {
            modelComboBox.addElement(lang);
        }
        langueComboBox.setModel(modelComboBox);
    }

//    public void addComboBoxElement(Member animator)
//    {
//        animatorsComboBox.addItem(animator);
//        this.errorAnimatorLabel.setVisible(false);
//        this.manageAnimatorButton.setEnabled(true);
//    }

    /**
     * Initialise items in expert combo box
     */
    public void initComboBoxExpert()
    {
        List<Expert> experts = ExpertController.getInstance().getAll();

        if(experts != null && !experts.isEmpty())
        {
            DefaultComboBoxModel<Expert> modelComboBox = new DefaultComboBoxModel<>();
            for(Expert unMember : experts)
            {
                modelComboBox.addElement(unMember);
            }
            expertsComboBox.setModel(modelComboBox);
        }
        else
        {
            this.errorExpertLabel.setText("Aucun Expert");
            this.errorExpertLabel.setVisible(true);
            this.manageExpertButton.setEnabled(false);
        }
    }

    // METHODE EVENEMENTIELLE

    /**
     * Permet de quitter l'application
     * Appelée : lors du clic sur le bouton Close
     */
    private void closeButtonActionPerformed(ActionEvent e)
    {
        ConfirmForm confirmView = new ConfirmForm(this,"Confirmation", "Voulez-vous quitter l'application ?");
        confirmView.initTextLanguage();
        confirmView.setVisible(true);
        if (confirmView.isAnswer())
        {
            System.exit(0);
        }
    }

    /**
     * Permet de gérer le hackathon sélectionné
     * Appelée : lors du clic sur le bouton Manage pour un hackathon
     */
    private void manageButtonActionPerformed(ActionEvent e)
    {
        //récupération du hackathon sélectionné
        Hackathon hackathonSelected = (Hackathon) hackathonsComboBox.getSelectedItem();

        // parametrage et affichage de la vue viewHackathonManagement
        this.viewHackathonManagement.setHackathon(hackathonSelected, this);
        this.viewHackathonManagement.setVisible(true);

    }

    /**
     * Permet de gérer le jury sélectionné
     * Appelée : lors du clic sur le bouton Manage pour un jury
     */
    private void manageJuryButtonActionPerformed(ActionEvent e)
    {
        //récupération du hackathon sélectionné
        Member jurySelected = (Member) juriesComboBox.getSelectedItem();

        // parametrage et affichage de la vue viewMemberForm
        this.viewMemberForm.initManaged(jurySelected, "Jury", this);
        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer le animateur sélectionné
     * Appelée : lors du clic sur le bouton Manage pour un animateur
     */
    private void manageAnimatorButtonActionPerformed(ActionEvent e)
    {
        //récupération du hackathon sélectionné
        Member animatorSelected = (Member) animatorsComboBox.getSelectedItem();

        // parametrage et affichage de la vue viewMemberForm
        this.viewMemberForm.initManaged(animatorSelected, "Animator",this);
        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer le animateur sélectionné
     * Appelée : lors du clic sur le bouton Manage pour un animateur
     */
    private void manageExpertButtonActionPerformed(ActionEvent e)
    {
        //récupération du hackathon sélectionné
        Expert selectedExpert = (Expert) expertsComboBox.getSelectedItem();

        // parametrage et affichage de la vue viewMemberForm
        this.viewMemberForm.setVisibleJobPart(true);
        this.viewMemberForm.initManaged(selectedExpert, "Expert", this);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer l'organisateur sélectionné
     * Appelée : lors du clic sur le bouton Manage pour un jury
     */
    private void manageOrganisateurButtonActionPerformed(ActionEvent e) {
        Member organisateurSelected = (Member) organisateurComboBox.getSelectedItem();

        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.initManaged(organisateurSelected, "Organisateur",this);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer un nouvel hackathon
     * Appelée : lors du clic sur le bouton New Hackathon
     */
    private void newHackaButtonActionPerformed(ActionEvent e)
    {
        this.viewHackathonManagement.setHackathon(null, this);
        this.viewHackathonManagement.setVisible(true);
    }

    /**
     * Permet de gérer un nouveau jury
     * Appelée : lors du clic sur le bouton New Jury
     */
    private void newJuryButtonActionPerformed(ActionEvent e)
    {
        this.viewMemberForm.initManaged(null, "Jury", this);
        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer un nouveau animateur
     * Appelée : lors du clic sur le bouton New Animator
     */
    private void newAnimatorButtonActionPerformed(ActionEvent e)
    {
        this.viewMemberForm.initManaged(null, "Animator",this);
        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer un nouveau expert
     * Appelée : lors du clic sur le bouton New Expert
     */
    private void newExpertButtonActionPerformed(ActionEvent e)
    {
        this.viewMemberForm.initManaged(null, "Expert", this);
        this.viewMemberForm.setVisibleJobPart(true);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer un nouveau organisateur
     * Appelée : lors du clic sur le bouton New Organizer
     */
    private void newOrganisateurButtonActionPerformed(ActionEvent e)
    {
        this.viewMemberForm.initManaged(null, "Organisateur", this);
        this.viewMemberForm.setVisibleJobPart(false);
        this.viewMemberForm.setVisible(true);
    }

    /**
     * Permet de gérer le language courant
     * Appelée : lors du clic sur la combo box de langue
     * @param e
     */
    private void langueComboBoxActionPerformed(ActionEvent e)
    {
        GlobalController.getInstance().loadLanguageByName((String) langueComboBox.getSelectedItem());

        initTextLanguage();
    }

    /**
     * permet d'ajouter un jury a la combo box hackathon
     * @param hackathon
     */
    public void addHackathonComboBox(Hackathon hackathon)
    {
        this.hackathonsComboBox.addItem(hackathon);
    }

    /**
     * permet de supprimer un hackathon a hackathon combo box
     * @param hackathon
     */
    public void removeHackathonInComboBox(Hackathon hackathon)
    {
        this.hackathonsComboBox.removeItem(hackathon);
    }

    /**
     * permet d'ajouter un jury a la combo box jury
     * @param member
     */
    public void addJuryComboBox(Member member)
    {
        this.juriesComboBox.addItem(member);
    }

    /**
     * permet d'ajouter un animateur a la combo box animateur
     * @param member
     */
    public void addAnimatorComboBox(Member member)
    {
        this.animatorsComboBox.addItem(member);
    }

    /**
     * permet d'ajouter un expert dans la combo box expert
     * @param expert
     */
    public void addExpertInExpertComboBox(Expert expert)
    {
        this.expertsComboBox.addItem(expert);
    }

    /**
     * permet de supprmer un organisateur a la combo box expert
     * @param expert
     */
    public void removeExpertInExpertComboBox(Expert expert)
    {
        this.expertsComboBox.removeItem(expert);
    }

    /**
     * permet d'ajouter un organisateur a la combo box organisateur
     * @param member
     */
    public void addOrganisateurComboBox(Member member)
    {
        this.organisateurComboBox.addItem(member);
    }

    /**
     * permet d'initialiser les champ de texte au language choisit (default: englais)
     */
    private void initTextLanguage()
    {
        manageAnimatorButton.setText(GlobalController.getInstance().getProperty("manageBTN"));
        manageExpertButton.setText(GlobalController.getInstance().getProperty("manageBTN"));
        manageButton.setText(GlobalController.getInstance().getProperty("manageBTN"));
        manageJuryButton.setText(GlobalController.getInstance().getProperty("manageBTN"));
        newHackathonButton.setText(GlobalController.getInstance().getProperty("newHackathonBtn"));
        newAnimatorButton.setText(GlobalController.getInstance().getProperty("newAnimatorBtn"));
        newJuryButton.setText(GlobalController.getInstance().getProperty("newJuryBtn"));
        newExpertButton.setText(GlobalController.getInstance().getProperty("newExpertBtn"));
        closeButton.setText(GlobalController.getInstance().getProperty("closeBtn"));
        newOrganisateurButton.setText(GlobalController.getInstance().getProperty("newOrganizerBtn"));
        manageOrganisateurButton.setText(GlobalController.getInstance().getProperty("manageBTN"));
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        hackathonLabel = new JLabel();
        errorLabel = new JLabel();
        hackathonsComboBox = new JComboBox();
        manageButton = new JButton();
        newHackathonButton = new JButton();
        vSpacer1 = new JPanel(null);
        juryLabel = new JLabel();
        errorJuryLabel = new JLabel();
        juriesComboBox = new JComboBox();
        manageJuryButton = new JButton();
        newJuryButton = new JButton();
        vSpacer2 = new JPanel(null);
        animatorLabel = new JLabel();
        errorAnimatorLabel = new JLabel();
        animatorsComboBox = new JComboBox();
        manageAnimatorButton = new JButton();
        newAnimatorButton = new JButton();
        vSpacer3 = new JPanel(null);
        expertLabel = new JLabel();
        errorExpertLabel = new JLabel();
        manageExpertButton = new JButton();
        expertsComboBox = new JComboBox();
        newExpertButton = new JButton();
        vSpacer4 = new JPanel(null);
        oragnisateurLabe = new JLabel();
        errorOrganisateurLabel = new JLabel();
        organisateurComboBox = new JComboBox();
        manageOrganisateurButton = new JButton();
        newOrganisateurButton = new JButton();
        buttonBar = new JPanel();
        langueComboBox = new JComboBox();
        hSpacer1 = new JPanel(null);
        closeButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(14, 37));
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new MigLayout(
                    "insets dialog,hidemode 3,alignx center",
                    // columns
                    "[253,fill]rel" +
                    "[fill]",
                    // rows
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]"));

                //---- hackathonLabel ----
                hackathonLabel.setText("Hackathon");
                contentPanel.add(hackathonLabel, "cell 0 0");

                //---- errorLabel ----
                errorLabel.setText("Error Message");
                contentPanel.add(errorLabel, "cell 0 1");
                contentPanel.add(hackathonsComboBox, "cell 0 2");

                //---- manageButton ----
                manageButton.setText("Manage");
                manageButton.addActionListener(e -> manageButtonActionPerformed(e));
                contentPanel.add(manageButton, "cell 1 2");

                //---- newHackathonButton ----
                newHackathonButton.setText("New Hackathon");
                newHackathonButton.addActionListener(e -> newHackaButtonActionPerformed(e));
                contentPanel.add(newHackathonButton, "cell 0 4");
                contentPanel.add(vSpacer1, "cell 0 5,hmin 30");

                //---- juryLabel ----
                juryLabel.setText("Jury");
                contentPanel.add(juryLabel, "cell 0 7");

                //---- errorJuryLabel ----
                errorJuryLabel.setText("Error Message");
                contentPanel.add(errorJuryLabel, "cell 0 8");
                contentPanel.add(juriesComboBox, "cell 0 9");

                //---- manageJuryButton ----
                manageJuryButton.setText("Manage");
                manageJuryButton.addActionListener(e -> manageJuryButtonActionPerformed(e));
                contentPanel.add(manageJuryButton, "cell 1 9");

                //---- newJuryButton ----
                newJuryButton.setText("New Jury");
                newJuryButton.addActionListener(e -> newJuryButtonActionPerformed(e));
                contentPanel.add(newJuryButton, "cell 0 10");
                contentPanel.add(vSpacer2, "cell 0 11,hmin 30");

                //---- animatorLabel ----
                animatorLabel.setText("Animator");
                contentPanel.add(animatorLabel, "cell 0 12");

                //---- errorAnimatorLabel ----
                errorAnimatorLabel.setText("Error Message");
                contentPanel.add(errorAnimatorLabel, "cell 0 13");
                contentPanel.add(animatorsComboBox, "cell 0 14");

                //---- manageAnimatorButton ----
                manageAnimatorButton.setText("Manage");
                manageAnimatorButton.addActionListener(e -> manageAnimatorButtonActionPerformed(e));
                contentPanel.add(manageAnimatorButton, "cell 1 14");

                //---- newAnimatorButton ----
                newAnimatorButton.setText("New Animator");
                newAnimatorButton.addActionListener(e -> newAnimatorButtonActionPerformed(e));
                contentPanel.add(newAnimatorButton, "cell 0 15");
                contentPanel.add(vSpacer3, "cell 0 16,hmin 30");

                //---- expertLabel ----
                expertLabel.setText("Expert");
                contentPanel.add(expertLabel, "cell 0 17");

                //---- errorExpertLabel ----
                errorExpertLabel.setText("Error Message");
                contentPanel.add(errorExpertLabel, "cell 0 18");

                //---- manageExpertButton ----
                manageExpertButton.setText("Manage");
                manageExpertButton.addActionListener(e -> manageExpertButtonActionPerformed(e));
                contentPanel.add(manageExpertButton, "cell 1 19");
                contentPanel.add(expertsComboBox, "cell 0 19");

                //---- newExpertButton ----
                newExpertButton.setText("New Expert");
                newExpertButton.addActionListener(e -> newExpertButtonActionPerformed(e));
                contentPanel.add(newExpertButton, "cell 0 20");
                contentPanel.add(vSpacer4, "cell 0 21,hmin 30");

                //---- oragnisateurLabe ----
                oragnisateurLabe.setText("Organisateur");
                contentPanel.add(oragnisateurLabe, "cell 0 22");

                //---- errorOrganisateurLabel ----
                errorOrganisateurLabel.setText("Error Message");
                contentPanel.add(errorOrganisateurLabel, "cell 0 23");
                contentPanel.add(organisateurComboBox, "cell 0 24");

                //---- manageOrganisateurButton ----
                manageOrganisateurButton.setText("Manage");
                manageOrganisateurButton.addActionListener(e -> manageOrganisateurButtonActionPerformed(e));
                contentPanel.add(manageOrganisateurButton, "cell 1 24");

                //---- newOrganisateurButton ----
                newOrganisateurButton.setText("New organizer");
                newOrganisateurButton.addActionListener(e -> newOrganisateurButtonActionPerformed(e));
                contentPanel.add(newOrganisateurButton, "cell 0 25");
            }
            dialogPane.add(contentPanel, BorderLayout.NORTH);

            //======== buttonBar ========
            {
                buttonBar.setLayout(new MigLayout(
                    "insets dialog,alignx right",
                    // columns
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[fill]" +
                    "[button,fill]",
                    // rows
                    null));

                //---- langueComboBox ----
                langueComboBox.addActionListener(e -> langueComboBoxActionPerformed(e));
                buttonBar.add(langueComboBox, "cell 0 0 4 1");
                buttonBar.add(hSpacer1, "cell 4 0 10 1,wmin 400");

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, "cell 14 0");
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel hackathonLabel;
    private JLabel errorLabel;
    private JComboBox hackathonsComboBox;
    private JButton manageButton;
    private JButton newHackathonButton;
    private JPanel vSpacer1;
    private JLabel juryLabel;
    private JLabel errorJuryLabel;
    private JComboBox juriesComboBox;
    private JButton manageJuryButton;
    private JButton newJuryButton;
    private JPanel vSpacer2;
    private JLabel animatorLabel;
    private JLabel errorAnimatorLabel;
    private JComboBox animatorsComboBox;
    private JButton manageAnimatorButton;
    private JButton newAnimatorButton;
    private JPanel vSpacer3;
    private JLabel expertLabel;
    private JLabel errorExpertLabel;
    private JButton manageExpertButton;
    private JComboBox expertsComboBox;
    private JButton newExpertButton;
    private JPanel vSpacer4;
    private JLabel oragnisateurLabe;
    private JLabel errorOrganisateurLabel;
    private JComboBox organisateurComboBox;
    private JButton manageOrganisateurButton;
    private JButton newOrganisateurButton;
    private JPanel buttonBar;
    private JComboBox langueComboBox;
    private JPanel hSpacer1;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


}
