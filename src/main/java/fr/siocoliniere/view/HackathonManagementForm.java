package fr.siocoliniere.view;

import fr.siocoliniere.bll.GlobalController;
import fr.siocoliniere.bo.Hackathon;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementForm extends JDialog
{

    private Hackathon hackathonManaged;
    private HackathonManagementSettingsForm viewHackathonManageSettings;
    private HackathonManagementMemberForm viewHackathonManageJury;
    private WelcomeForm welcomeFormView;

    /**
     * constructeur de la class hackathonManagementForm
     */
    public HackathonManagementForm()
    {
        this.hackathonManaged = null;

        // window settings
        setPreferredSize(new Dimension(300, 300));
        setLocation(500, 500);
        setModal(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();
        initComponents();

        viewHackathonManageSettings = new HackathonManagementSettingsForm(this);
        viewHackathonManageJury = new HackathonManagementMemberForm(this);

        viewHackathonManageSettings.setVisible(false);
        viewHackathonManageJury.setVisible(false);

    }

    /**
     * Permet de valoriser le hackathon à configurer
     * @param hackathon
     */
    public void setHackathon(Hackathon hackathon, WelcomeForm welcomeView)
    {
        initTextLanguage();

        this.welcomeFormView = welcomeView;
        this.hackathonManaged = hackathon;

        if (this.hackathonManaged != null)
        {
            setTitle("Hackat'Orga : " + this.hackathonManaged.getName());
            juryButton.setEnabled(true);
            animatorButton.setEnabled(true);
            expertButton.setEnabled(true);
        }
        else
        {
            juryButton.setEnabled(false);
            animatorButton.setEnabled(false);
            expertButton.setEnabled(false);

            setTitle("Hackat'Orga : " + GlobalController.getInstance().getProperty("newHackathonBtn"));
        }

    }

    /**
     * Permet de quitter l'application
     * Appelée : lors du clic sur le bouton Close
     */
    private void closeButtonActionPerformed(ActionEvent e)
    {
        this.setVisible(false);
    }

    /**
     * Permet de paramétrer les settings du Hackathon
     * Appelée : lors du clic sur le bouton Settings
     */
    private void settingsButtonActionPerformed(ActionEvent e)
    {
        viewHackathonManageSettings.initHackathonManaged(this.hackathonManaged, this, this.welcomeFormView);
        viewHackathonManageSettings.setVisible(true);
    }

    /**
     * Permet de paramétrer les jury du Hackathon
     * Appelée : lors du clic sur le bouton Jury
     */
    private void juryButtonActionPerformed(ActionEvent e)
    {
        viewHackathonManageJury.initHackathonManaged(this.hackathonManaged, "jury");
        viewHackathonManageJury.setVisible(true);
    }

    /**
     * Permet de paramétrer les animateurs du hackthon
     * Appelée : lors du clic sur le bouton expert
     */
    private void animatorButtonActionPerformed(ActionEvent e)
    {
        viewHackathonManageJury.initHackathonManaged(this.hackathonManaged, "animator");
        viewHackathonManageJury.setVisible(true);
    }

    /**
     * Permet de parametrer les experts du hackathon
     * Appelée : lors du clic sur le bouton expert
     */
    private void expertButtonActionPerformed(ActionEvent e)
    {
        viewHackathonManageJury.initHackathonManaged(hackathonManaged, "expert");
        viewHackathonManageJury.setVisible(true);
    }

    /**
     * setter sur l'attribut enable du boutton jury
     */
     public void setEnableJuryButton(boolean state)
     {
        juryButton.setEnabled(state);
     }

    /**
     * setter sur l'attribut enable du boutton animator
     */
     public void setEnableAnimatorButton(boolean state)
     {
         animatorButton.setEnabled(state);
     }

    /**
     * setter sur l'attribut enable du boutton expert
     */
     public void setEnableExpertButton(boolean state)
     {
         expertButton.setEnabled(state);
     }

    /**
     *  setter sur l'attribut hackathonManaged
     */
    public void setHackathonManaged(Hackathon hackathon)
    {
        this.hackathonManaged = hackathon;
    }

    /**
     * permet d'initialiser les champ de texte au language choisit (default: englais)
     */
    private void initTextLanguage()
    {
        settingsButton.setText(GlobalController.getInstance().getProperty("settingBtn"));
        juryButton.setText(GlobalController.getInstance().getProperty("juryBtn"));
        animatorButton.setText(GlobalController.getInstance().getProperty("animatorBtn"));
        expertButton.setText(GlobalController.getInstance().getProperty("expertBtn"));
        closeButton.setText(GlobalController.getInstance().getProperty("closeBtn"));
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        settingsButton = new JButton();
        juryButton = new JButton();
        animatorButton = new JButton();
        expertButton = new JButton();
        buttonBar = new JPanel();
        closeButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                //---- settingsButton ----
                settingsButton.setText("Settings");
                settingsButton.addActionListener(e -> settingsButtonActionPerformed(e));
                contentPanel.add(settingsButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- juryButton ----
                juryButton.setText("Jury");
                juryButton.addActionListener(e -> juryButtonActionPerformed(e));
                contentPanel.add(juryButton, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- animatorButton ----
                animatorButton.setText("Animator");
                animatorButton.addActionListener(e -> animatorButtonActionPerformed(e));
                contentPanel.add(animatorButton, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- expertButton ----
                expertButton.setText("Expert");
                expertButton.addActionListener(e -> expertButtonActionPerformed(e));
                contentPanel.add(expertButton, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JButton settingsButton;
    private JButton juryButton;
    private JButton animatorButton;
    private JButton expertButton;
    private JPanel buttonBar;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
