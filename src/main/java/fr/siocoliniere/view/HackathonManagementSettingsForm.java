package fr.siocoliniere.view;

import fr.siocoliniere.bll.GlobalController;
import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.OrganisateurController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementSettingsForm extends JDialog {

    private Hackathon hackathonManaged;
    private HackathonManagementForm hackathonFormView;
    private int number;
    private WelcomeForm welcomeFormView;

    /**
     * Constructeur
     *
     * @param owner
     */
    public HackathonManagementSettingsForm(Window owner) {
        super(owner);

        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(500, 600));

        setTitle("Hackat'Orga : " + GlobalController.getInstance().getProperty("settingBtn"));

        initComponents();
    }

    /**
     * Permet d'initialiser la vue pour un hacakthon choisi
     *
     * @param hackathonManaged
     */

    public void initHackathonManaged(Hackathon hackathonManaged, HackathonManagementForm manageFormView, WelcomeForm welcomeview)
    {
        initTextLanguage();
        this.hackathonFormView = manageFormView;
        this.hackathonManaged = hackathonManaged;
        this.welcomeFormView = welcomeview;

        String name = "";
        String topic = "";
        String description = "";
        String place = "";
        String target = "";
        int day = 1;
        int month = 1;
        int years = 2022;
        int number = 2;

        if(this.hackathonManaged != null)
        {
            name = hackathonManaged.getName();
            topic = hackathonManaged.getTopic();
            description = hackathonManaged.getDescription();
            place = hackathonManaged.getPlace();
            target = hackathonManaged.getTarget();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(hackathonManaged.getDate());

            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            years = calendar.get(Calendar.YEAR);
            number = hackathonManaged.getNbrParticipants();
            deleteButton.setVisible(true);
        }
        else
        {
            deleteButton.setVisible(false);
        }

        SpinnerNumberModel spinner = new SpinnerNumberModel(number, 2, 999999999, 1);
        nameTextField.setText(name);
        topicTextField.setText(topic);
        descriptionTextArea.setText(description);
        placeTextField.setText(place);
        targetTextField.setText(target);
        daySpinner.setValue(day);
        monthSpinner.setValue(month);
        yearSpinner.setValue(years);
        nbrParticipantsSpiner.setModel(spinner);

        descriptionTextArea.setLineWrap(true);
        errorIndicationHachathon.setForeground(Color.black);
        errorIndicationHachathon.setText(GlobalController.getInstance().getProperty("needTextField"));

        initRoomComboBox();
        initOrganisateurComboBox();
        clearBorderComponent();
    }

    /**
     * permet de clear la couleur des bordures des JComponent
     */
    private void clearBorderComponent()
    {
        JComponent[] components = {nameTextField, topicTextField, placeTextField, targetTextField, descriptionTextArea, yearSpinner, monthSpinner, daySpinner};

        for(JComponent component : components)
        {
            component.setBorder(BorderFactory.createLineBorder(Color.black));
        }
    }

    /**
     * permet d'initialiser les valeurs de la combo box organisateur
     */
    private void initOrganisateurComboBox()
    {
        DefaultComboBoxModel<Member> modelComboOrganisateur = new DefaultComboBoxModel<>();

        for (Member organisateur : OrganisateurController.getInstance().getAll())
        {
            modelComboOrganisateur.addElement(organisateur);
        }
        organisateurComboBox.setModel(modelComboOrganisateur);
    }

    /**
     * permet d'initialiser les valeur de la combo box room
     */
    private void initRoomComboBox()
    {
        DefaultComboBoxModel<String> modelComboRoom = new DefaultComboBoxModel<>();

        for (String room : HackathonController.getInstance().getRoom())
        {
            modelComboRoom.addElement(room);
        }
        roomBox.setModel(modelComboRoom);
    }

    /**
     * Permet de sauvegarder les settings du hackathon
     * Appelée : lors du clic sur le bouton Save
     */
    private void saveButtonActionPerformed(ActionEvent e)
    {
        clearBorderComponent();

        String name = nameTextField.getText().trim();
        String topic = topicTextField.getText().trim();
        String place = placeTextField.getText().trim();
        String target = targetTextField.getText().trim();
        String description = descriptionTextArea.getText().trim();
        String room = (String) roomBox.getSelectedItem();
        int nbParticipant = (Integer) nbrParticipantsSpiner.getValue();
        Member organisateur = (Member) organisateurComboBox.getSelectedItem();
        String date = daySpinner.getValue().toString() + "/" + ((int) monthSpinner.getValue() + 1) + "/" + yearSpinner.getValue().toString();


        if(name.equals("") || topic.equals("") || place.equals("") || target.equals("") || description.equals("") || organisateur == null)
        {
            errorIndicationHachathon.setText(GlobalController.getInstance().getProperty("textFieldNotFill"));
            errorIndicationHachathon.setForeground(Color.red);

            if(name.equals(""))
            {
                nameTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(topic.equals(""))
            {
                topicTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(place.equals(""))
            {
                placeTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(target.equals(""))
            {
                targetTextField.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(description.equals(""))
            {
                descriptionTextArea.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            if(organisateurComboBox.getSelectedItem() == null)
            {
                organisateurComboBox.setBorder(BorderFactory.createLineBorder(Color.red));
            }
        }
        else
        {
            if(!GlobalController.getInstance().isValidDate(date))
            {
                yearSpinner.setBorder(BorderFactory.createLineBorder(Color.red));
                monthSpinner.setBorder(BorderFactory.createLineBorder(Color.red));
                daySpinner.setBorder(BorderFactory.createLineBorder(Color.red));
            }
            else
            {
                int day = Integer.parseInt(date.split("/")[0]);
                int month = Integer.parseInt(date.split("/")[1]);
                int years = Integer.parseInt(date.split("/")[2]);

                Calendar myCalendar = new GregorianCalendar(years, month -1 , day);
                Date dateHackathon = myCalendar.getTime();

                Hackathon newHackathonManaged = HackathonController.getInstance().saveHackathonStandalone(hackathonManaged, name, topic, description, place, target, room, nbParticipant, dateHackathon, organisateur);
                welcomeFormView.addHackathonComboBox(newHackathonManaged);
                if (newHackathonManaged != null)
                {
                    this.hackathonFormView.setEnableJuryButton(true);
                    this.hackathonFormView.setEnableAnimatorButton(true);
                    this.hackathonFormView.setEnableExpertButton(true);
                }

                this.hackathonFormView.setHackathonManaged(newHackathonManaged);
                this.dispose();
            }
        }
    }

    /**
     * ferme la vue
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e)
    {
        this.dispose();
    }

    private void deleteButtonActionPerformed(ActionEvent e)
    {
        ConfirmForm confirmView = new ConfirmForm(this, "Confirmation", "Voulez-vous supprimer cet Hackathon ?");
        confirmView.setVisible(true);

        if (confirmView.isAnswer())
        {
            HackathonController.getInstance().deleteHackathon(hackathonManaged);
            welcomeFormView.removeHackathonInComboBox(hackathonManaged);
            confirmView.setVisible(false);
            this.setVisible(false);
            hackathonFormView.setVisible(false);
        }
    }


    /**
     * permet d'initialiser les champ de texte au language choisit (default: englais)
     */
    private void initTextLanguage() {
        nameLabel.setText(GlobalController.getInstance().getProperty("nameLabel"));
        topicLabel.setText(GlobalController.getInstance().getProperty("topicLabel"));
        placeLabel.setText(GlobalController.getInstance().getProperty("locationlabel"));
        targetLabel.setText(GlobalController.getInstance().getProperty("goalLabel"));
        dateLabel.setText(GlobalController.getInstance().getProperty("dateLabel"));
        descriptionLabel.setText(GlobalController.getInstance().getProperty("descriptionLabel"));
        roomLabel.setText(GlobalController.getInstance().getProperty("inscription"));
        saveButton.setText(GlobalController.getInstance().getProperty("saveBtn"));
        cancelButton.setText(GlobalController.getInstance().getProperty("cancelBtn"));
        setTitle("Hackat'Orga : " + GlobalController.getInstance().getProperty("settingBtn"));
    }


    /**
     * JFormDesigner - DO NOT MODIFY
     */


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        nameLabel = new JLabel();
        nameTextField = new JTextField();
        vSpacer3 = new JPanel(null);
        topicLabel = new JLabel();
        topicTextField = new JTextField();
        vSpacer2 = new JPanel(null);
        placeLabel = new JLabel();
        placeTextField = new JTextField();
        vSpacer1 = new JPanel(null);
        targetLabel = new JLabel();
        targetTextField = new JTextField();
        vSpacer7 = new JPanel(null);
        dateLabel = new JLabel();
        daySpinner = new JSpinner();
        monthSpinner = new JSpinner();
        yearSpinner = new JSpinner();
        vSpacer4 = new JPanel(null);
        descriptionLabel = new JLabel();
        vSpacer5 = new JPanel(null);
        roomBox = new JComboBox();
        descriptionScrollPane = new JScrollPane();
        descriptionTextArea = new JTextArea();
        descriptionLabel2 = new JLabel();
        nbrParticipantsSpiner = new JSpinner();
        vSpacer6 = new JPanel(null);
        organisateurLabel = new JLabel();
        organisateurComboBox = new JComboBox();
        vSpacer8 = new JPanel(null);
        roomLabel = new JLabel();
        buttonBar = new JPanel();
        errorIndicationHachathon = new JLabel();
        deleteButton = new JButton();
        saveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 25, 0, 26, 27, 26, 30, 0, 0, 27, 97, 24, 0, 22, 0, 0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

                //---- nameLabel ----
                nameLabel.setText("Name*");
                contentPanel.add(nameLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(nameTextField, new GridBagConstraints(1, 1, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                contentPanel.add(vSpacer3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- topicLabel ----
                topicLabel.setText("Topic*");
                contentPanel.add(topicLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(topicTextField, new GridBagConstraints(1, 3, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- placeLabel ----
                placeLabel.setText("Location*");
                contentPanel.add(placeLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(placeTextField, new GridBagConstraints(1, 5, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer1, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- targetLabel ----
                targetLabel.setText("Goal*");
                contentPanel.add(targetLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(targetTextField, new GridBagConstraints(1, 7, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 0, 15, 0), 0, 0));
                contentPanel.add(vSpacer7, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- dateLabel ----
                dateLabel.setText("date*");
                contentPanel.add(dateLabel, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- daySpinner ----
                daySpinner.setModel(new SpinnerNumberModel(1, 1, 31, 1));
                contentPanel.add(daySpinner, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- monthSpinner ----
                monthSpinner.setModel(new SpinnerNumberModel(1, 1, 12, 1));
                contentPanel.add(monthSpinner, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- yearSpinner ----
                yearSpinner.setModel(new SpinnerNumberModel(2021, 2021, null, 1));
                contentPanel.add(yearSpinner, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(vSpacer4, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- descriptionLabel ----
                descriptionLabel.setText("Description*");
                contentPanel.add(descriptionLabel, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(vSpacer5, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(roomBox, new GridBagConstraints(1, 17, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //======== descriptionScrollPane ========
                {
                    descriptionScrollPane.setViewportView(descriptionTextArea);
                }
                contentPanel.add(descriptionScrollPane, new GridBagConstraints(1, 11, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- descriptionLabel2 ----
                descriptionLabel2.setText("Participants*");
                contentPanel.add(descriptionLabel2, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(nbrParticipantsSpiner, new GridBagConstraints(1, 13, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                contentPanel.add(vSpacer6, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- organisateurLabel ----
                organisateurLabel.setText("Organisateur*");
                contentPanel.add(organisateurLabel, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(organisateurComboBox, new GridBagConstraints(1, 15, 8, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                contentPanel.add(vSpacer8, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- roomLabel ----
                roomLabel.setText("Inscription*");
                contentPanel.add(roomLabel, new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.NONE,
                    new Insets(0, 10, 5, 5), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.NORTH);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {84, 0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0};
                buttonBar.add(errorIndicationHachathon, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- deleteButton ----
                deleteButton.setText("Supprimer");
                deleteButton.addActionListener(e -> deleteButtonActionPerformed(e));
                buttonBar.add(deleteButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- saveButton ----
                saveButton.setText("Save");
                saveButton.addActionListener(e -> saveButtonActionPerformed(e));
                buttonBar.add(saveButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JPanel vSpacer3;
    private JLabel topicLabel;
    private JTextField topicTextField;
    private JPanel vSpacer2;
    private JLabel placeLabel;
    private JTextField placeTextField;
    private JPanel vSpacer1;
    private JLabel targetLabel;
    private JTextField targetTextField;
    private JPanel vSpacer7;
    private JLabel dateLabel;
    private JSpinner daySpinner;
    private JSpinner monthSpinner;
    private JSpinner yearSpinner;
    private JPanel vSpacer4;
    private JLabel descriptionLabel;
    private JPanel vSpacer5;
    private JComboBox roomBox;
    private JScrollPane descriptionScrollPane;
    private JTextArea descriptionTextArea;
    private JLabel descriptionLabel2;
    private JSpinner nbrParticipantsSpiner;
    private JPanel vSpacer6;
    private JLabel organisateurLabel;
    private JComboBox organisateurComboBox;
    private JPanel vSpacer8;
    private JLabel roomLabel;
    private JPanel buttonBar;
    private JLabel errorIndicationHachathon;
    private JButton deleteButton;
    private JButton saveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

